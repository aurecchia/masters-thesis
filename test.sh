#!/usr/bin/env bash

BASEPATH="/home/auri/repos/viperproject/silver/src/test/resources/quantifiedpermissions/misc/"
FILES=$(find $BASEPATH -type f -name "*.sil" | sort)

printf "%-37s %7s %6s %4s %4s  %s\n" "FILE" "unknown" "UNSAT" "SAT" "RET" "unknown-reason"
for f in $FILES;
do
  out=$(./silicon.sh --ideModeAdvanced --numberOfParallelVerifiers 1 --z3Exe /usr/local/Viper/z3/bin/z3 $f)
  ret=$?

  unknowns=$(echo "$out" | grep "^unknown" | wc -l)
  unsats=$(echo "$out" | grep "^unsat" | wc -l)
  sats=$(echo "$out" | grep "^sat" | wc -l)

  reason=$(echo "$out" | grep "reason-unknown" | grep -Eo "\"[()a-z\ -]+\"" | sort | uniq | tr --delete '\n')

  filename="${f##*/}"
  printf "%-37s %7d %6d %4d %4s  %s\n" "$filename" "$unknowns" "$unsats" "$sats" "$ret" "$reason" 
done;


