// ===== Preamble (resources/preamble.als) =====
open util/boolean
open util/ternary
open util/integer
open util/relation
sig Snap {}
one sig Unit extends Snap {}
sig SortWrapper extends Snap {
    wrapped: one univ
}
pred sortwrapper_new [ e: univ, sw: Snap] {
    sw in SortWrapper
    sw.wrapped = e
}
abstract sig Combine extends Snap {
    left: one Snap,
    right: one Snap
}
pred combine [ l, r: Snap, c: Combine ] {
    c.left = l && c.right = r
}
// ===== Perms (resources/perms.als) =====
abstract sig Perm {
    num: one Int,
    denom: one Int
} {
    num >= 0
    denom > 0
    // Silicon does not define this in SMT and defining it here would prevent us
    // from having sums of permissions that exceed W
    // num <= denom
}
one sig W in Perm {} {
    num = 1
    denom = 1
}
one sig Z in Perm {} {
    num = 0
    denom = 1
}
pred perm_new[ n, d: Int, p': Perm ] {
    p'.num = n
    p'.denom = d
}
pred perm_less[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] < mul[p2.num, p1.denom]
}
pred perm_at_most[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] <= mul[p2.num, p1.denom]
}
pred perm_at_least[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] >= mul[p2.num, p1.denom]
}
pred perm_greater[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] > mul[p2.num, p1.denom]
}
pred perm_plus[ p1, p2, p': Perm ] {
  (p1.denom = p2.denom)
    =>
        (p'.num = plus[p1.num, p2.num] &&
         p'.denom = p1.denom)
    else
        (p'.num = plus[mul[p1.num, p2.denom], mul[p2.num, p1.denom]] &&
         p'.denom = mul[p1.denom, p2.denom])
}
pred perm_minus[ p1, p2, p': Perm ] {
	perm_equals[ p1, p2 ]
		=> p' = Z
		else (
		  p'.num = minus[mul[p1.num, p2.denom], mul[p2.num, p1.denom]] and
		  p'.denom = mul[p1.denom, p2.denom]
		)
}
pred int_perm_div[ p: Perm, d: Int, p': Perm ] {
  p'.num = p.num
  p'.denom = mul[p.denom, d]
}
pred perm_mul[ p1, p2, p': Perm ] {
  p'.num = mul[p1.num, p2.num]
  p'.denom = mul[p1.denom, p2.denom]
}
pred int_perm_mul[ i: Int, p, p': Perm ] {
  p'.num = mul[p.num, i]
  p'.denom = p.denom
}
pred perm_min[ p1, p2, p': Perm ] {
  mul[p1.num, p2.denom] < mul[p2.num, p1.denom]
    => (p' = p1)
    else (p' = p2)
}
pred perm_equals [ p1, p2: Perm ] {
	// p1 = p2
  // The additional two clauses create problems with quantified permissions
  (p1.num = p2.num && p1.denom = p2.denom)
  || (mul[p1.num, p2.denom] = mul[p1.denom, p2.num])
}
// ===== Sets (resources/set_fun.als) =====
abstract sig Set {
	set_elems: set univ
}
pred empty_set [ s': Set ] {
	no s'.set_elems
}
pred set_singleton [ e: univ, s': Set ] {
	s'.set_elems = e
	one e
}
pred set_add [ s1: Set, e: univ, s': Set ] {
	s'.set_elems = s1.set_elems + e
	one e
}
fun set_cardinality [ s1: Set ]: one Int {
	#(s1.set_elems)
}
pred set_difference [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems - s2.set_elems
}
pred set_intersection [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems & s2.set_elems
}
pred set_union [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems + s2.set_elems
}
pred set_in [ e: univ, s1: Set ] {
	e in s1.set_elems
	one e
	some s1.set_elems
}
pred set_subset [ s1, s2: Set ] {
	s1.set_elems in s2.set_elems
}
pred set_disjoint [ s1, s2: Set ] {
	disjoint[s1.set_elems, s2.set_elems]
}
pred set_equals [ s1, s2: Set ] {
	s1.set_elems = s2.set_elems
}
fact { all s1, s2: Set | s1.set_elems = s2.set_elems <=> s1 = s2 }
// ===== Seqs (resources/seq.als) =====
abstract sig Seq {
	// seq_rel: Int -> lone univ
	seq_rel: seq univ
} {
	isSeq[seq_rel]
}
pred seq_ranged [ from, to: Int, s': Seq ] {
	// { all i: Int | 0 <= i && i < sub[to, from] => s[i] = plus[from, i] }
	// #s = sub[to, from]
	s'.seq_rel = subseq[iden, from, sub[to, 1]]
}
pred seq_singleton [ e: univ, s': Seq ] {
	s'.seq_rel[0] = e
	#(s'.seq_rel) = 1
}
// NOTE: The sequence resulting from the wrapped 'append' operation may be
//		 truncated if the sequences are too long.
pred seq_append [ s1, s2, s': Seq ] {
	s'.seq_rel = append[s1.seq_rel, s2.seq_rel]
}
fun seq_length [ s: Seq ]: one Int {
	#(s.seq_rel)
}
fun seq_at [ s: Seq, i: Int ]: one univ {
	s.seq_rel[i]
}
pred seq_take [ s: Seq, i: Int, s': Seq ] {
	let to = sub[i, 1] |
	s'.seq_rel = subseq[ s.seq_rel, 0, to]
}
pred seq_drop [ s: Seq, i: Int, s': Seq ] {
	let to = sub[#s.seq_rel, 1] |
	s'.seq_rel = subseq[ s.seq_rel, i, to ]
}
pred seq_in [ s1: Seq, e: univ ] {
	e in elems[s1.seq_rel]
}
pred seq_update [ s: Seq, i: Int, e: univ, s': Seq ] {
	s'.seq_rel = setAt[s.seq_rel, i, e]
} 
// ===== Multiset (resources/multiset.als) =====
abstract sig Multiset {
	ms_elems: univ -> lone Int
} {
	all i: univ.ms_elems | gt[i, 0]
}
pred empty_multiset [ ms': Multiset ] {
    no ms'.ms_elems
}
pred multiset_singleton [ e: univ, ms': Multiset ] {
	ms'.ms_elems = (e -> 1)
}
pred multiset_add [ ms1: Multiset, elem: univ, ms': Multiset ] {
	ms'.ms_elems =	{ e: univ, v: Int | e in (elem - dom[ms1.ms_elems]) and v = 1 } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] - elem) and v = ms1.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] & elem) and v = add[ms1.ms_elems[e], 1] }
}
fun multiset_cardinality_fun [ ms: Multiset ]: one Int {
    sum ms.ms_elems[univ]
}
pred multiset_cardinality [ ms: Multiset, card: Int ] {
	card = (let s = { c: Int, e: univ | (e -> c) in ms.ms_elems } |
					(sum i: (s).univ | mul[#(s[i]), i]) )
	card >= 0
}
pred multiset_difference [ ms1, ms2, ms': Multiset ] {
	ms'.ms_elems = { e: univ, v: Int | e in (dom[ms1.ms_elems] - dom[ms2.ms_elems]) and v = ms1.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and
									e.(ms2.ms_elems) < e.(ms1.ms_elems) and
									v = minus[e.(ms1.ms_elems), e.(ms2.ms_elems)] }
}
pred multiset_intersection [ ms1, ms2, ms': Multiset ] {
	ms'.ms_elems = { e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and v = min[e.(ms1.ms_elems) + e.(ms2.ms_elems)] }
}
pred multiset_union [ ms1, ms2, ms': Multiset ] {
	ms'.ms_elems = { e: univ, v: Int | e in (dom[ms2.ms_elems] - dom[ms1.ms_elems]) and v = ms2.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] - dom[ms2.ms_elems]) and v = ms1.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and v = add[ms1.ms_elems[e], ms2.ms_elems[e]] }
}
pred multiset_subset [ ms1, ms2: Multiset ] {
	dom[ms1.ms_elems] in dom[ms2.ms_elems]
	{ all e: dom[ms1.ms_elems] | ms1.ms_elems[e] <= ms2.ms_elems[e] }
}
pred multiset_count [ ms1: Multiset, e: univ, c: Int ] {
	c = ms1.ms_elems[e]
} 
fun multiset_count_fun [ ms1: Multiset, e: univ ]: one Int {
	ms1.ms_elems[e]
} 
pred multiset_equals [ ms1, ms2: Multiset ] {
    ms1.ms_elems = ms2.ms_elems
}
sig Ref {
  next: lone Ref, 
  refTypedFields': set Ref
} {
  refTypedFields' = next
}

one sig NULL extends Ref {}
fact { NULL.refTypedFields' = none && no NULL.next }

one sig Store {
  ns1': one Set_Ref, 
  ns2': one Set_Ref, 
  n1': one Ref, 
  n2': one Ref, 
  refTypedVars': set Ref
} {
  refTypedVars' = n1' + n2'
}
one sig ns1_78_01 in Set_Ref {}
one sig ns2_79_01 in Set_Ref {}
one sig n1_80_01 in Ref {}
one sig n2_81_01 in Ref {}
fact { Store.ns1' = ns1_78_01 }
fact { Store.ns2' = ns2_79_01 }
fact { Store.n1' = n1_80_01 }
fact { Store.n2' = n2_81_01 }

// Heap Chunks
// QA r@85@01 :: ((r@85@01 in ns1@78@01) ==> (inv@86@01(r@85@01) == r@85@01))
fact { (all r_85_01: Ref | (set_in[r_85_01, ns1_78_01] => (Fun.inv_86_01[r_85_01] = r_85_01))) }
// QA r :: ((inv@86@01(r) in ns1@78@01) ==> (inv@86@01(r) == r))
fact { (all r: Ref | (set_in[Fun.inv_86_01[r], ns1_78_01] => (Fun.inv_86_01[r] = r))) }
one sig t_83_01 in FVF_Ref {}
fact { all r: Ref | (set_in[Fun.inv_86_01[r], ns1_78_01] implies W else Z) = PermFun.next[r, t_83_01] }
// QA r@94@01 :: ((r@94@01 in ns2@79@01) ==> (inv@95@01(r@94@01) == r@94@01))
fact { (all r_94_01: Ref | (set_in[r_94_01, ns2_79_01] => (Fun.inv_95_01[r_94_01] = r_94_01))) }
// QA r :: ((inv@95@01(r) in ns2@79@01) ==> (inv@95@01(r) == r))
fact { (all r: Ref | (set_in[Fun.inv_95_01[r], ns2_79_01] => (Fun.inv_95_01[r] = r))) }
one sig t_92_01 in FVF_Ref {}
fact { all r: Ref | (set_in[Fun.inv_95_01[r], ns2_79_01] implies W else Z) = PermFun.next[r, t_92_01] }
fact { all r: Ref | (some fvf: (t_83_01 + t_92_01) | one PermFun.next[r, fvf] and perm_less[Z, PermFun.next[r, fvf]]) <=> (one r.next) }
fact { all r: Ref, fvf: (t_83_01 + t_92_01) | one PermFun.next[r, fvf] => (perm_at_most[PermFun.next[r, fvf], W]) }
one sig Preds {}

// Path Conditions
// ($t@91@01 == Combine(SortWrapper($t@92@01, Snap), $t@93@01))
one sig t_91_01 in Snap {}
one sig temp_0' in Snap {}
one sig temp_1' in Snap {}
one sig t_93_01 in Snap {}
fact { sortwrapper_new[t_92_01, temp_1'] && 
       combine[temp_1', t_93_01, temp_0'] && 
       (t_91_01 = temp_0') }
// QA r@94@01 :: ((r@94@01 in ns2@79@01) ==> !((r@94@01 == Null)))
fact { (all r_94_01: Ref | (set_in[r_94_01, ns2_79_01] => !((r_94_01 = NULL)))) }
// ($t@93@01 == Combine(_, _))
one sig temp_2' in Snap {}
fact { combine[Unit, Unit, temp_2'] && 
       (t_93_01 = temp_2') }
// QA r@85@01 :: ((r@85@01 in ns1@78@01) ==> (inv@86@01(r@85@01) == r@85@01))
fact { (all r_85_01: Ref | (set_in[r_85_01, ns1_78_01] => (Fun.inv_86_01[r_85_01] = r_85_01))) }
// ($t@82@01 == Combine(SortWrapper($t@83@01, Snap), $t@84@01))
one sig t_82_01 in Snap {}
one sig temp_3' in Snap {}
one sig temp_4' in Snap {}
one sig t_84_01 in Snap {}
fact { sortwrapper_new[t_83_01, temp_4'] && 
       combine[temp_4', t_84_01, temp_3'] && 
       (t_82_01 = temp_3') }
// QA r@85@01 :: ((r@85@01 in ns1@78@01) ==> !((r@85@01 == Null)))
fact { (all r_85_01: Ref | (set_in[r_85_01, ns1_78_01] => !((r_85_01 = NULL)))) }
// QA r :: ((inv@86@01(r) in ns1@78@01) ==> (Lookup(next, sm@89@01(), r) == Lookup(next, $t@83@01, r)))
one sig sm_89_01 in FVF_Ref {}
fact { (all r: Ref | (set_in[Fun.inv_86_01[r], ns1_78_01] => (Lookup.next[sm_89_01, r] = Lookup.next[t_83_01, r]))) }
// (n1@80@01 in ns1@78@01)
fact { set_in[n1_80_01, ns1_78_01] }
// ($t@87@01 == Combine(_, $t@90@01))
one sig t_87_01 in Snap {}
one sig temp_5' in Snap {}
one sig t_90_01 in Snap {}
fact { combine[Unit, t_90_01, temp_5'] && 
       (t_87_01 = temp_5') }
// QA r@94@01 :: ((r@94@01 in ns2@79@01) ==> (inv@95@01(r@94@01) == r@94@01))
fact { (all r_94_01: Ref | (set_in[r_94_01, ns2_79_01] => (Fun.inv_95_01[r_94_01] = r_94_01))) }
// ($t@84@01 == Combine(_, $t@87@01))
one sig temp_6' in Snap {}
fact { combine[Unit, t_87_01, temp_6'] && 
       (t_84_01 = temp_6') }
// QA r :: ((inv@95@01(r) in ns2@79@01) ==> (inv@95@01(r) == r))
fact { (all r: Ref | (set_in[Fun.inv_95_01[r], ns2_79_01] => (Fun.inv_95_01[r] = r))) }
// (n2@81@01 in ns2@79@01)
fact { set_in[n2_81_01, ns2_79_01] }
// ($t@90@01 == Combine(_, $t@91@01))
one sig temp_7' in Snap {}
fact { combine[Unit, t_91_01, temp_7'] && 
       (t_90_01 = temp_7') }
// QA r@88@01 :: ((r@88@01 in ns1@78@01) && !((Lookup(next, sm@89@01(), r@88@01) == Null)) ==> (Lookup(next, sm@89@01(), r@88@01) in ns1@78@01))
fact { (all r_88_01: Ref | ((set_in[r_88_01, ns1_78_01] && !((Lookup.next[sm_89_01, r_88_01] = NULL))) => set_in[Lookup.next[sm_89_01, r_88_01], ns1_78_01])) }
// (SetCardinality:(ns1@78@01) >= 3)
fact { (set_cardinality[ns1_78_01] >= 3) }
// QA r :: ((inv@86@01(r) in ns1@78@01) ==> (inv@86@01(r) == r))
fact { (all r: Ref | (set_in[Fun.inv_86_01[r], ns1_78_01] => (Fun.inv_86_01[r] = r))) }
// (SetCardinality:(ns2@79@01) >= 3)
fact { (set_cardinality[ns2_79_01] >= 3) }

// Permission functions
one sig PermFun {
  next: (Ref -> FVF_Ref -> one Perm)
}

// Functions
one sig Fun {
  inv_86_01: (Ref -> lone Ref), 
  inv_95_01: (Ref -> lone Ref)
}

// Lookup functions
one sig Lookup {
  next: (FVF_Ref -> Ref -> lone Ref)
}
fact { all fvf: FVF_Ref, r: Ref | (one PermFun.next[r, fvf] and perm_less[Z, PermFun.next[r, fvf]]) => (Lookup.next[fvf, r] = r.next) }
// Other sorts
sig Set_Ref extends Set {} {
  set_elems in Ref
}
sig FVF_Ref {}

// No object unreachable from the Store
fact { Ref = Store.refTypedVars'.*refTypedFields' + NULL + (SortWrapper.wrapped <: Ref) + Store.ns1'.set_elems + Store.ns2'.set_elems }

// Signarure Restrictions
fact { Set = Store.ns1' + Store.ns2' }
fact { FVF_Ref = t_83_01 + t_92_01 + sm_89_01 }
fact { Snap = t_91_01 + temp_0' + temp_1' + t_93_01 + temp_2' + t_82_01 + temp_3' + temp_4' + t_84_01 + t_87_01 + temp_5' + t_90_01 + temp_6' + temp_7' + Unit }
fact { Perm = PermFun.next[Ref, FVF_Ref] + W + Z }
fact { Seq = none }
fact { Multiset = none }

run {} for 10 but 4 int, 2 Set, 3 FVF_Ref, 11 Snap, 3 Perm