// ===== Preamble (resources/preamble.als) =====
open util/boolean
open util/ternary
open util/integer
open util/relation
sig Snap {}
one sig Unit extends Snap {}
sig SortWrapper extends Snap {
    wrapped: one univ
}
pred sortwrapper_new [ e: univ, sw: Snap] {
    sw in SortWrapper
    sw.wrapped = e
}
abstract sig Combine extends Snap {
    left: one Snap,
    right: one Snap
}
pred combine [ l, r: Snap, c: Combine ] {
    c.left = l && c.right = r
}
// ===== Perms (resources/perms.als) =====
abstract sig Perm {
    num: one Int,
    denom: one Int
} {
    num >= 0
    denom > 0
    // Silicon does not define this in SMT and defining it here would prevent us
    // from having sums of permissions that exceed W
    // num <= denom
}
one sig W in Perm {} {
    num = 1
    denom = 1
}
one sig Z in Perm {} {
    num = 0
    denom = 1
}
pred perm_new[ n, d: Int, p': Perm ] {
    p'.num = n
    p'.denom = d
}
pred perm_less[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] < mul[p2.num, p1.denom]
}
pred perm_at_most[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] <= mul[p2.num, p1.denom]
}
pred perm_at_least[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] >= mul[p2.num, p1.denom]
}
pred perm_greater[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] > mul[p2.num, p1.denom]
}
pred perm_plus[ p1, p2, p': Perm ] {
  (p1.denom = p2.denom)
    =>
        (p'.num = plus[p1.num, p2.num] &&
         p'.denom = p1.denom)
    else
        (p'.num = plus[mul[p1.num, p2.denom], mul[p2.num, p1.denom]] &&
         p'.denom = mul[p1.denom, p2.denom])
}
pred perm_minus[ p1, p2, p': Perm ] {
	perm_equals[ p1, p2 ]
		=> p' = Z
		else (
		  p'.num = minus[mul[p1.num, p2.denom], mul[p2.num, p1.denom]] and
		  p'.denom = mul[p1.denom, p2.denom]
		)
}
pred int_perm_div[ p: Perm, d: Int, p': Perm ] {
  p'.num = p.num
  p'.denom = mul[p.denom, d]
}
pred perm_mul[ p1, p2, p': Perm ] {
  p'.num = mul[p1.num, p2.num]
  p'.denom = mul[p1.denom, p2.denom]
}
pred int_perm_mul[ i: Int, p, p': Perm ] {
  p'.num = mul[p.num, i]
  p'.denom = p.denom
}
pred perm_min[ p1, p2, p': Perm ] {
  mul[p1.num, p2.denom] < mul[p2.num, p1.denom]
    => (p' = p1)
    else (p' = p2)
}
pred perm_equals [ p1, p2: Perm ] {
	// p1 = p2
  // The additional two clauses create problems with quantified permissions
  (p1.num = p2.num && p1.denom = p2.denom)
  || (mul[p1.num, p2.denom] = mul[p1.denom, p2.num])
}
// ===== Sets (resources/set_fun.als) =====
abstract sig Set {
	set_elems: set univ
}
pred empty_set [ s': Set ] {
	no s'.set_elems
}
pred set_singleton [ e: univ, s': Set ] {
	s'.set_elems = e
	one e
}
pred set_add [ s1: Set, e: univ, s': Set ] {
	s'.set_elems = s1.set_elems + e
	one e
}
fun set_cardinality [ s1: Set ]: one Int {
	#(s1.set_elems)
}
pred set_difference [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems - s2.set_elems
}
pred set_intersection [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems & s2.set_elems
}
pred set_union [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems + s2.set_elems
}
pred set_in [ e: univ, s1: Set ] {
	e in s1.set_elems
	one e
	some s1.set_elems
}
pred set_subset [ s1, s2: Set ] {
	s1.set_elems in s2.set_elems
}
pred set_disjoint [ s1, s2: Set ] {
	disjoint[s1.set_elems, s2.set_elems]
}
pred set_equals [ s1, s2: Set ] {
	s1.set_elems = s2.set_elems
}
fact { all s1, s2: Set | s1.set_elems = s2.set_elems <=> s1 = s2 }
// ===== Seqs (resources/seq.als) =====
abstract sig Seq {
	// seq_rel: Int -> lone univ
	seq_rel: seq univ
} {
	isSeq[seq_rel]
}
pred seq_ranged [ from, to: Int, s': Seq ] {
	// { all i: Int | 0 <= i && i < sub[to, from] => s[i] = plus[from, i] }
	// #s = sub[to, from]
	s'.seq_rel = subseq[iden, from, sub[to, 1]]
}
pred seq_singleton [ e: univ, s': Seq ] {
	s'.seq_rel[0] = e
	#(s'.seq_rel) = 1
}
// NOTE: The sequence resulting from the wrapped 'append' operation may be
//		 truncated if the sequences are too long.
pred seq_append [ s1, s2, s': Seq ] {
	s'.seq_rel = append[s1.seq_rel, s2.seq_rel]
}
fun seq_length [ s: Seq ]: one Int {
	#(s.seq_rel)
}
fun seq_at [ s: Seq, i: Int ]: one univ {
	s.seq_rel[i]
}
pred seq_take [ s: Seq, i: Int, s': Seq ] {
	let to = sub[i, 1] |
	s'.seq_rel = subseq[ s.seq_rel, 0, to]
}
pred seq_drop [ s: Seq, i: Int, s': Seq ] {
	let to = sub[#s.seq_rel, 1] |
	s'.seq_rel = subseq[ s.seq_rel, i, to ]
}
pred seq_in [ s1: Seq, e: univ ] {
	e in elems[s1.seq_rel]
}
pred seq_update [ s: Seq, i: Int, e: univ, s': Seq ] {
	s'.seq_rel = setAt[s.seq_rel, i, e]
} 
// ===== Multiset (resources/multiset.als) =====
abstract sig Multiset {
	ms_elems: univ -> lone Int
} {
	all i: univ.ms_elems | gt[i, 0]
}
pred empty_multiset [ ms': Multiset ] {
    no ms'.ms_elems
}
pred multiset_singleton [ e: univ, ms': Multiset ] {
	ms'.ms_elems = (e -> 1)
}
pred multiset_add [ ms1: Multiset, elem: univ, ms': Multiset ] {
	ms'.ms_elems =	{ e: univ, v: Int | e in (elem - dom[ms1.ms_elems]) and v = 1 } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] - elem) and v = ms1.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] & elem) and v = add[ms1.ms_elems[e], 1] }
}
fun multiset_cardinality_fun [ ms: Multiset ]: one Int {
    sum ms.ms_elems[univ]
}
pred multiset_cardinality [ ms: Multiset, card: Int ] {
	card = (let s = { c: Int, e: univ | (e -> c) in ms.ms_elems } |
					(sum i: (s).univ | mul[#(s[i]), i]) )
	card >= 0
}
pred multiset_difference [ ms1, ms2, ms': Multiset ] {
	ms'.ms_elems = { e: univ, v: Int | e in (dom[ms1.ms_elems] - dom[ms2.ms_elems]) and v = ms1.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and
									e.(ms2.ms_elems) < e.(ms1.ms_elems) and
									v = minus[e.(ms1.ms_elems), e.(ms2.ms_elems)] }
}
pred multiset_intersection [ ms1, ms2, ms': Multiset ] {
	ms'.ms_elems = { e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and v = min[e.(ms1.ms_elems) + e.(ms2.ms_elems)] }
}
pred multiset_union [ ms1, ms2, ms': Multiset ] {
	ms'.ms_elems = { e: univ, v: Int | e in (dom[ms2.ms_elems] - dom[ms1.ms_elems]) and v = ms2.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] - dom[ms2.ms_elems]) and v = ms1.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and v = add[ms1.ms_elems[e], ms2.ms_elems[e]] }
}
pred multiset_subset [ ms1, ms2: Multiset ] {
	dom[ms1.ms_elems] in dom[ms2.ms_elems]
	{ all e: dom[ms1.ms_elems] | ms1.ms_elems[e] <= ms2.ms_elems[e] }
}
pred multiset_count [ ms1: Multiset, e: univ, c: Int ] {
	c = ms1.ms_elems[e]
} 
fun multiset_count_fun [ ms1: Multiset, e: univ ]: one Int {
	ms1.ms_elems[e]
} 
pred multiset_equals [ ms1, ms2: Multiset ] {
    ms1.ms_elems = ms2.ms_elems
}
sig Ref {
  val: lone Int, 
  refTypedFields': set Ref
} {
  refTypedFields' = none
}

one sig NULL extends Ref {}
fact { NULL.refTypedFields' = none && no NULL.val }

one sig Store {
  ns': one Set_Ref, 
  n1': one Ref, 
  p1': one Perm, 
  p2': one Perm, 
  p3': one Perm, 
  refTypedVars': set Ref
} {
  refTypedVars' = n1'
}
one sig ns_87_01 in Set_Ref {}
one sig n1_88_01 in Ref {}
one sig p1_93_01 in Perm {}
one sig p2_94_01 in Perm {}
one sig p3_102_01 in Perm {}
fact { Store.ns' = ns_87_01 }
fact { Store.n1' = n1_88_01 }
fact { Store.p1' = p1_93_01 }
fact { Store.p2' = p2_94_01 }
fact { Store.p3' = p3_102_01 }

// Heap Chunks
// QA r@91@01 :: ((r@91@01 in ns@87@01) ==> (inv@92@01(r@91@01) == r@91@01))
fact { (all r_91_01: Ref | (set_in[r_91_01, ns_87_01] => (Fun.inv_92_01[r_91_01] = r_91_01))) }
// QA r :: ((inv@92@01(r) in ns@87@01) ==> (inv@92@01(r) == r))
fact { (all r: Ref | (set_in[Fun.inv_92_01[r], ns_87_01] => (Fun.inv_92_01[r] = r))) }
one sig t_90_01 in FVF_Int {}
one sig fresh_quantifier_vars_0 {
  temp_0': Ref -> lone Perm, 
  temp_2': Ref -> lone Perm, 
  temp_3': Ref -> lone Perm
}
fact { all r: Ref | perm_minus[(set_in[Fun.inv_92_01[r], ns_87_01] implies W else Z), PTAKEN.pTaken_98_01[r], fresh_quantifier_vars_0.temp_3'[r]] && 
       perm_minus[fresh_quantifier_vars_0.temp_3'[r], PTAKEN.pTaken_100_01[r], fresh_quantifier_vars_0.temp_2'[r]] && 
       perm_minus[fresh_quantifier_vars_0.temp_2'[r], PTAKEN.pTaken_103_01[r], fresh_quantifier_vars_0.temp_0'[r]] && 
       fresh_quantifier_vars_0.temp_0'[r] = PermFun.val[r, t_90_01] }
fact { all r: Ref | (some fvf: (t_90_01) | one PermFun.val[r, fvf] and perm_less[Z, PermFun.val[r, fvf]]) <=> (one r.val) }
fact { all r: Ref, fvf: (t_90_01) | one PermFun.val[r, fvf] => (perm_at_most[PermFun.val[r, fvf], W]) }
one sig Preds {}

// Path Conditions
// (p2@94@01 > Z)
fact { perm_greater[p2_94_01, Z] }
// ($t@96@01 == Combine(_, _))
one sig t_96_01 in Snap {}
one sig temp_0' in Snap {}
fact { combine[Unit, Unit, temp_0'] && 
       (t_96_01 = temp_0') }
// QA r :: ((inv@92@01(r) in ns@87@01) ==> (inv@92@01(r) == r))
fact { (all r: Ref | (set_in[Fun.inv_92_01[r], ns_87_01] => (Fun.inv_92_01[r] = r))) }
// (p3@102@01 == (((inv@92@01(n1@88@01) in ns@87@01) ? W : Z - pTaken@98@01(n1@88@01)) - pTaken@100@01(n1@88@01)))
one sig temp_1' in Perm {}
one sig temp_2' in Perm {}
fact { perm_minus[(set_in[Fun.inv_92_01[n1_88_01], ns_87_01] implies W else Z), PTAKEN.pTaken_98_01[n1_88_01], temp_2'] && 
       perm_minus[temp_2', PTAKEN.pTaken_100_01[n1_88_01], temp_1'] && 
       perm_equals[p3_102_01, temp_1'] }
// ((p1@93@01 + p2@94@01) < W)
one sig temp_3' in Perm {}
fact { perm_plus[p1_93_01, p2_94_01, temp_3'] && 
       perm_less[temp_3', W] }
// (p3@102@01 > Z)
fact { perm_greater[p3_102_01, Z] }
// ((Z < ((inv@92@01(n1@88@01) in ns@87@01) ? W : Z - pTaken@98@01(n1@88@01))) ==> (Lookup(val, sm@101@01(), n1@88@01) == Lookup(val, $t@90@01, n1@88@01)))
one sig temp_4' in Perm {}
one sig sm_101_01 in FVF_Int {}
fact { perm_minus[(set_in[Fun.inv_92_01[n1_88_01], ns_87_01] implies W else Z), PTAKEN.pTaken_98_01[n1_88_01], temp_4'] && 
       (perm_less[Z, temp_4'] => (Lookup.val[sm_101_01, n1_88_01] = Lookup.val[t_90_01, n1_88_01])) }
// ($t@89@01 == Combine(SortWrapper($t@90@01, Snap), _))
one sig t_89_01 in Snap {}
one sig temp_5' in Snap {}
one sig temp_6' in Snap {}
fact { sortwrapper_new[t_90_01, temp_6'] && 
       combine[temp_6', Unit, temp_5'] && 
       (t_89_01 = temp_5') }
// (n1@88@01 in ns@87@01)
fact { set_in[n1_88_01, ns_87_01] }
// ((Z < (((inv@92@01(n1@88@01) in ns@87@01) ? W : Z - pTaken@98@01(n1@88@01)) - pTaken@100@01(n1@88@01))) ==> (Lookup(val, sm@104@01(), n1@88@01) == Lookup(val, $t@90@01, n1@88@01)))
one sig temp_7' in Perm {}
one sig temp_8' in Perm {}
one sig sm_104_01 in FVF_Int {}
fact { perm_minus[(set_in[Fun.inv_92_01[n1_88_01], ns_87_01] implies W else Z), PTAKEN.pTaken_98_01[n1_88_01], temp_8'] && 
       perm_minus[temp_8', PTAKEN.pTaken_100_01[n1_88_01], temp_7'] && 
       (perm_less[Z, temp_7'] => (Lookup.val[sm_104_01, n1_88_01] = Lookup.val[t_90_01, n1_88_01])) }
// QA r@91@01 :: ((r@91@01 in ns@87@01) ==> !((r@91@01 == Null)))
fact { (all r_91_01: Ref | (set_in[r_91_01, ns_87_01] => !((r_91_01 = NULL)))) }
// QA r@91@01 :: ((r@91@01 in ns@87@01) ==> (inv@92@01(r@91@01) == r@91@01))
fact { (all r_91_01: Ref | (set_in[r_91_01, ns_87_01] => (Fun.inv_92_01[r_91_01] = r_91_01))) }
// ($t@97@01 == _)
one sig t_97_01 in Snap {}
fact { (t_97_01 = Unit) }
// ((inv@92@01(n1@88@01) in ns@87@01) ==> (Lookup(val, sm@99@01(), n1@88@01) == Lookup(val, $t@90@01, n1@88@01)))
one sig sm_99_01 in FVF_Int {}
fact { (set_in[Fun.inv_92_01[n1_88_01], ns_87_01] => (Lookup.val[sm_99_01, n1_88_01] = Lookup.val[t_90_01, n1_88_01])) }
// (p1@93@01 > Z)
fact { perm_greater[p1_93_01, Z] }

// Permission functions
one sig PermFun {
  val: (Ref -> FVF_Int -> one Perm)
}

// Functions
one sig Fun {
  inv_92_01: (Ref -> lone Ref)
}

// Lookup functions
one sig Lookup {
  val: (FVF_Int -> Ref -> lone Int)
}
fact { all fvf: FVF_Int, r: Ref | (one PermFun.val[r, fvf] and perm_less[Z, PermFun.val[r, fvf]]) => (Lookup.val[fvf, r] = r.val) }
// Other sorts
sig Set_Ref extends Set {} {
  set_elems in Ref
}
sig FVF_Int {}

// Macros
// QA r :: (pTaken@98@01(r) == (r == n1@88@01) ? ((inv@92@01(r) in ns@87@01) ? W : Z PermMin p1@93@01) : Z)
one sig fresh_quantifier_vars_1 {
  temp_0': Ref -> lone Perm
}
fact { (all r: Ref | perm_min[(set_in[Fun.inv_92_01[r], ns_87_01] implies W else Z), p1_93_01, fresh_quantifier_vars_1.temp_0'[r]] && perm_equals[PTAKEN.pTaken_98_01[r], ((r = n1_88_01) implies fresh_quantifier_vars_1.temp_0'[r] else Z)]) }
// QA r :: (pTaken@100@01(r) == (r == n1@88@01) ? (((inv@92@01(r) in ns@87@01) ? W : Z - pTaken@98@01(r)) PermMin p2@94@01) : Z)
one sig fresh_quantifier_vars_2 {
  temp_0': Ref -> lone Perm, 
  temp_2': Ref -> lone Perm
}
fact { (all r: Ref | perm_minus[(set_in[Fun.inv_92_01[r], ns_87_01] implies W else Z), PTAKEN.pTaken_98_01[r], fresh_quantifier_vars_2.temp_2'[r]] && perm_min[fresh_quantifier_vars_2.temp_2'[r], p2_94_01, fresh_quantifier_vars_2.temp_0'[r]] && perm_equals[PTAKEN.pTaken_100_01[r], ((r = n1_88_01) implies fresh_quantifier_vars_2.temp_0'[r] else Z)]) }
// QA r :: (pTaken@103@01(r) == (r == n1@88@01) ? ((((inv@92@01(r) in ns@87@01) ? W : Z - pTaken@98@01(r)) - pTaken@100@01(r)) PermMin p3@102@01) : Z)
one sig fresh_quantifier_vars_3 {
  temp_0': Ref -> lone Perm, 
  temp_2': Ref -> lone Perm, 
  temp_3': Ref -> lone Perm
}
fact { (all r: Ref | perm_minus[(set_in[Fun.inv_92_01[r], ns_87_01] implies W else Z), PTAKEN.pTaken_98_01[r], fresh_quantifier_vars_3.temp_3'[r]] && perm_minus[fresh_quantifier_vars_3.temp_3'[r], PTAKEN.pTaken_100_01[r], fresh_quantifier_vars_3.temp_2'[r]] && perm_min[fresh_quantifier_vars_3.temp_2'[r], p3_102_01, fresh_quantifier_vars_3.temp_0'[r]] && perm_equals[PTAKEN.pTaken_103_01[r], ((r = n1_88_01) implies fresh_quantifier_vars_3.temp_0'[r] else Z)]) }
one sig PTAKEN {
  pTaken_98_01: Ref -> one Perm, 
  pTaken_100_01: Ref -> one Perm, 
  pTaken_103_01: Ref -> one Perm
}

// No object unreachable from the Store
fact { Ref = Store.refTypedVars'.*refTypedFields' + NULL + (SortWrapper.wrapped <: Ref) + Store.ns'.set_elems }

// Signarure Restrictions
fact { Set = Store.ns' }
fact { Perm = Store.p1' + Store.p2' + Store.p3' + temp_1' + temp_2' + temp_3' + temp_4' + temp_7' + temp_8' + PermFun.val[Ref, FVF_Int] + W + Z }
fact { FVF_Int = t_90_01 + sm_101_01 + sm_104_01 + sm_99_01 }
fact { Snap = t_96_01 + temp_0' + t_89_01 + temp_5' + temp_6' + t_97_01 + Unit }
fact { Seq = none }
fact { Multiset = none }

run {} for 5 but 4 int, 1 Set, 6 Perm, 4 FVF_Int, 7 Snap
