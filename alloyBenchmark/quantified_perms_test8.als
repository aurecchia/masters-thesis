// ===== Preamble (resources/preamble.als) =====
open util/boolean
open util/ternary
open util/integer
open util/relation
sig Snap {}
one sig Unit extends Snap {}
sig SortWrapper extends Snap {
    wrapped: one univ
}
pred sortwrapper_new [ e: univ, sw: Snap] {
    sw in SortWrapper
    sw.wrapped = e
}
abstract sig Combine extends Snap {
    left: one Snap,
    right: one Snap
}
pred combine [ l, r: Snap, c: Combine ] {
    c.left = l && c.right = r
}
// ===== Perms (resources/perms.als) =====
abstract sig Perm {
    num: one Int,
    denom: one Int
} {
    num >= 0
    denom > 0
    // Silicon does not define this in SMT and defining it here would prevent us
    // from having sums of permissions that exceed W
    // num <= denom
}
one sig W in Perm {} {
    num = 1
    denom = 1
}
one sig Z in Perm {} {
    num = 0
    denom = 1
}
pred perm_new[ n, d: Int, p': Perm ] {
    p'.num = n
    p'.denom = d
}
pred perm_less[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] < mul[p2.num, p1.denom]
}
pred perm_at_most[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] <= mul[p2.num, p1.denom]
}
pred perm_at_least[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] >= mul[p2.num, p1.denom]
}
pred perm_greater[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] > mul[p2.num, p1.denom]
}
pred perm_plus[ p1, p2, p': Perm ] {
  (p1.denom = p2.denom)
    =>
        (p'.num = plus[p1.num, p2.num] &&
         p'.denom = p1.denom)
    else
        (p'.num = plus[mul[p1.num, p2.denom], mul[p2.num, p1.denom]] &&
         p'.denom = mul[p1.denom, p2.denom])
}
pred perm_minus[ p1, p2, p': Perm ] {
	perm_equals[ p1, p2 ]
		=> p' = Z
		else (
		  p'.num = minus[mul[p1.num, p2.denom], mul[p2.num, p1.denom]] and
		  p'.denom = mul[p1.denom, p2.denom]
		)
}
pred int_perm_div[ p: Perm, d: Int, p': Perm ] {
  p'.num = p.num
  p'.denom = mul[p.denom, d]
}
pred perm_mul[ p1, p2, p': Perm ] {
  p'.num = mul[p1.num, p2.num]
  p'.denom = mul[p1.denom, p2.denom]
}
pred int_perm_mul[ i: Int, p, p': Perm ] {
  p'.num = mul[p.num, i]
  p'.denom = p.denom
}
pred perm_min[ p1, p2, p': Perm ] {
  mul[p1.num, p2.denom] < mul[p2.num, p1.denom]
    => (p' = p1)
    else (p' = p2)
}
pred perm_equals [ p1, p2: Perm ] {
	// p1 = p2
  // The additional two clauses create problems with quantified permissions
  (p1.num = p2.num && p1.denom = p2.denom)
  || (mul[p1.num, p2.denom] = mul[p1.denom, p2.num])
}
// ===== Sets (resources/set_fun.als) =====
abstract sig Set {
	set_elems: set univ
}
pred empty_set [ s': Set ] {
	no s'.set_elems
}
pred set_singleton [ e: univ, s': Set ] {
	s'.set_elems = e
	one e
}
pred set_add [ s1: Set, e: univ, s': Set ] {
	s'.set_elems = s1.set_elems + e
	one e
}
fun set_cardinality [ s1: Set ]: one Int {
	#(s1.set_elems)
}
pred set_difference [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems - s2.set_elems
}
pred set_intersection [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems & s2.set_elems
}
pred set_union [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems + s2.set_elems
}
pred set_in [ e: univ, s1: Set ] {
	e in s1.set_elems
	one e
	some s1.set_elems
}
pred set_subset [ s1, s2: Set ] {
	s1.set_elems in s2.set_elems
}
pred set_disjoint [ s1, s2: Set ] {
	disjoint[s1.set_elems, s2.set_elems]
}
pred set_equals [ s1, s2: Set ] {
	s1.set_elems = s2.set_elems
}
fact { all s1, s2: Set | s1.set_elems = s2.set_elems <=> s1 = s2 }
// ===== Seqs (resources/seq.als) =====
abstract sig Seq {
	// seq_rel: Int -> lone univ
	seq_rel: seq univ
} {
	isSeq[seq_rel]
}
pred seq_ranged [ from, to: Int, s': Seq ] {
	// { all i: Int | 0 <= i && i < sub[to, from] => s[i] = plus[from, i] }
	// #s = sub[to, from]
	s'.seq_rel = subseq[iden, from, sub[to, 1]]
}
pred seq_singleton [ e: univ, s': Seq ] {
	s'.seq_rel[0] = e
	#(s'.seq_rel) = 1
}
// NOTE: The sequence resulting from the wrapped 'append' operation may be
//		 truncated if the sequences are too long.
pred seq_append [ s1, s2, s': Seq ] {
	s'.seq_rel = append[s1.seq_rel, s2.seq_rel]
}
fun seq_length [ s: Seq ]: one Int {
	#(s.seq_rel)
}
fun seq_at [ s: Seq, i: Int ]: one univ {
	s.seq_rel[i]
}
pred seq_take [ s: Seq, i: Int, s': Seq ] {
	let to = sub[i, 1] |
	s'.seq_rel = subseq[ s.seq_rel, 0, to]
}
pred seq_drop [ s: Seq, i: Int, s': Seq ] {
	let to = sub[#s.seq_rel, 1] |
	s'.seq_rel = subseq[ s.seq_rel, i, to ]
}
pred seq_in [ s1: Seq, e: univ ] {
	e in elems[s1.seq_rel]
}
pred seq_update [ s: Seq, i: Int, e: univ, s': Seq ] {
	s'.seq_rel = setAt[s.seq_rel, i, e]
} 
// ===== Multiset (resources/multiset.als) =====
abstract sig Multiset {
	ms_elems: univ -> lone Int
} {
	all i: univ.ms_elems | gt[i, 0]
}
pred empty_multiset [ ms': Multiset ] {
    no ms'.ms_elems
}
pred multiset_singleton [ e: univ, ms': Multiset ] {
	ms'.ms_elems = (e -> 1)
}
pred multiset_add [ ms1: Multiset, elem: univ, ms': Multiset ] {
	ms'.ms_elems =	{ e: univ, v: Int | e in (elem - dom[ms1.ms_elems]) and v = 1 } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] - elem) and v = ms1.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] & elem) and v = add[ms1.ms_elems[e], 1] }
}
fun multiset_cardinality_fun [ ms: Multiset ]: one Int {
    sum ms.ms_elems[univ]
}
pred multiset_cardinality [ ms: Multiset, card: Int ] {
	card = (let s = { c: Int, e: univ | (e -> c) in ms.ms_elems } |
					(sum i: (s).univ | mul[#(s[i]), i]) )
	card >= 0
}
pred multiset_difference [ ms1, ms2, ms': Multiset ] {
	ms'.ms_elems = { e: univ, v: Int | e in (dom[ms1.ms_elems] - dom[ms2.ms_elems]) and v = ms1.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and
									e.(ms2.ms_elems) < e.(ms1.ms_elems) and
									v = minus[e.(ms1.ms_elems), e.(ms2.ms_elems)] }
}
pred multiset_intersection [ ms1, ms2, ms': Multiset ] {
	ms'.ms_elems = { e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and v = min[e.(ms1.ms_elems) + e.(ms2.ms_elems)] }
}
pred multiset_union [ ms1, ms2, ms': Multiset ] {
	ms'.ms_elems = { e: univ, v: Int | e in (dom[ms2.ms_elems] - dom[ms1.ms_elems]) and v = ms2.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] - dom[ms2.ms_elems]) and v = ms1.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and v = add[ms1.ms_elems[e], ms2.ms_elems[e]] }
}
pred multiset_subset [ ms1, ms2: Multiset ] {
	dom[ms1.ms_elems] in dom[ms2.ms_elems]
	{ all e: dom[ms1.ms_elems] | ms1.ms_elems[e] <= ms2.ms_elems[e] }
}
pred multiset_count [ ms1: Multiset, e: univ, c: Int ] {
	c = ms1.ms_elems[e]
} 
fun multiset_count_fun [ ms1: Multiset, e: univ ]: one Int {
	ms1.ms_elems[e]
} 
pred multiset_equals [ ms1, ms2: Multiset ] {
    ms1.ms_elems = ms2.ms_elems
}
sig Ref {
  next: lone Ref, 
  refTypedFields': set Ref
} {
  refTypedFields' = next
}

one sig NULL extends Ref {}
fact { NULL.refTypedFields' = none && no NULL.next }

one sig Store {
  ns1': one Set_Ref, 
  ns2': one Set_Ref, 
  n1': one Ref, 
  n2': one Ref, 
  refTypedVars': set Ref
} {
  refTypedVars' = n1' + n2'
}
one sig ns1_100_01 in Set_Ref {}
one sig ns2_101_01 in Set_Ref {}
one sig n1_102_01 in Ref {}
one sig n2_103_01 in Ref {}
fact { Store.ns1' = ns1_100_01 }
fact { Store.ns2' = ns2_101_01 }
fact { Store.n1' = n1_102_01 }
fact { Store.n2' = n2_103_01 }

// Heap Chunks
// QA r@109@01 :: ((r@109@01 in ns1@100@01) ==> (inv@110@01(r@109@01) == r@109@01))
fact { (all r_109_01: Ref | (set_in[r_109_01, ns1_100_01] => (Fun.inv_110_01[r_109_01] = r_109_01))) }
// QA r :: ((inv@110@01(r) in ns1@100@01) ==> (inv@110@01(r) == r))
fact { (all r: Ref | (set_in[Fun.inv_110_01[r], ns1_100_01] => (Fun.inv_110_01[r] = r))) }
one sig t_107_01 in FVF_Ref {}
fact { all r: Ref | (set_in[Fun.inv_110_01[r], ns1_100_01] implies W else Z) = PermFun.next[r, t_107_01] }
// QA r@121@01 :: ((r@121@01 in ns2@101@01) ==> (inv@122@01(r@121@01) == r@121@01))
fact { (all r_121_01: Ref | (set_in[r_121_01, ns2_101_01] => (Fun.inv_122_01[r_121_01] = r_121_01))) }
// QA r :: ((inv@122@01(r) in ns2@101@01) ==> (inv@122@01(r) == r))
fact { (all r: Ref | (set_in[Fun.inv_122_01[r], ns2_101_01] => (Fun.inv_122_01[r] = r))) }
one sig t_119_01 in FVF_Ref {}
fact { all r: Ref | (set_in[Fun.inv_122_01[r], ns2_101_01] implies W else Z) = PermFun.next[r, t_119_01] }
fact { all r: Ref | (some fvf: (t_107_01 + t_119_01) | one PermFun.next[r, fvf] and perm_less[Z, PermFun.next[r, fvf]]) <=> (one r.next) }
fact { all r: Ref, fvf: (t_107_01 + t_119_01) | one PermFun.next[r, fvf] => (perm_at_most[PermFun.next[r, fvf], W]) }
one sig Preds {}

// Path Conditions
// QA r@112@01 :: ((r@112@01 in ns1@100@01) && ((r@112@01 in ns1@100@01) ==> !((Lookup(next, sm@114@01(), r@112@01) == Null))) ==> (Lookup(next, sm@114@01(), r@112@01) in ns2@101@01))
one sig sm_114_01 in FVF_Ref {}
fact { (all r_112_01: Ref | ((set_in[r_112_01, ns1_100_01] && (set_in[r_112_01, ns1_100_01] => !((Lookup.next[sm_114_01, r_112_01] = NULL)))) => set_in[Lookup.next[sm_114_01, r_112_01], ns2_101_01])) }
// (SetCardinality:(ns2@101@01) >= 3)
fact { (set_cardinality[ns2_101_01] >= 3) }
// QA r@112@01 :: ((r@112@01 in ns1@100@01) && ((r@112@01 in ns1@100@01) ==> !((Lookup(next, sm@114@01(), r@112@01) == Null))) ==> ((r@112@01 in ns1@100@01) ==> !((Lookup(next, sm@114@01(), r@112@01) == Null))) && (r@112@01 in ns1@100@01))
fact { (all r_112_01: Ref | ((set_in[r_112_01, ns1_100_01] && (set_in[r_112_01, ns1_100_01] => !((Lookup.next[sm_114_01, r_112_01] = NULL)))) => ((set_in[r_112_01, ns1_100_01] => !((Lookup.next[sm_114_01, r_112_01] = NULL))) && set_in[r_112_01, ns1_100_01]))) }
// ($t@117@01 == Combine(SortWrapper($t@119@01, Snap), $t@120@01))
one sig t_117_01 in Snap {}
one sig temp_0' in Snap {}
one sig temp_1' in Snap {}
one sig t_120_01 in Snap {}
fact { sortwrapper_new[t_119_01, temp_1'] && 
       combine[temp_1', t_120_01, temp_0'] && 
       (t_117_01 = temp_0') }
// QA r@121@01 :: ((r@121@01 in ns2@101@01) ==> (inv@122@01(r@121@01) == r@121@01))
fact { (all r_121_01: Ref | (set_in[r_121_01, ns2_101_01] => (Fun.inv_122_01[r_121_01] = r_121_01))) }
// ($t@111@01 == Combine(_, $t@115@01))
one sig t_111_01 in Snap {}
one sig temp_2' in Snap {}
one sig t_115_01 in Snap {}
fact { combine[Unit, t_115_01, temp_2'] && 
       (t_111_01 = temp_2') }
// QA r@106@01 :: ((r@106@01 in ns1@100@01) ==> !((r@106@01 in ns2@101@01)))
fact { (all r_106_01: Ref | (set_in[r_106_01, ns1_100_01] => !(set_in[r_106_01, ns2_101_01]))) }
// QA r :: ((inv@110@01(r) in ns1@100@01) ==> (inv@110@01(r) == r))
fact { (all r: Ref | (set_in[Fun.inv_110_01[r], ns1_100_01] => (Fun.inv_110_01[r] = r))) }
// QA r :: ((inv@110@01(r) in ns1@100@01) ==> (Lookup(next, sm@114@01(), r) == Lookup(next, $t@107@01, r)))
fact { (all r: Ref | (set_in[Fun.inv_110_01[r], ns1_100_01] => (Lookup.next[sm_114_01, r] = Lookup.next[t_107_01, r]))) }
// (SetCardinality:(ns1@100@01) >= 3)
fact { (set_cardinality[ns1_100_01] >= 3) }
// QA r :: ((inv@122@01(r) in ns2@101@01) ==> (inv@122@01(r) == r))
fact { (all r: Ref | (set_in[Fun.inv_122_01[r], ns2_101_01] => (Fun.inv_122_01[r] = r))) }
// ($t@105@01 == Combine(SortWrapper($t@107@01, Snap), $t@108@01))
one sig t_105_01 in Snap {}
one sig temp_3' in Snap {}
one sig temp_4' in Snap {}
one sig t_108_01 in Snap {}
fact { sortwrapper_new[t_107_01, temp_4'] && 
       combine[temp_4', t_108_01, temp_3'] && 
       (t_105_01 = temp_3') }
// ($t@115@01 == Combine(_, $t@116@01))
one sig temp_5' in Snap {}
one sig t_116_01 in Snap {}
fact { combine[Unit, t_116_01, temp_5'] && 
       (t_115_01 = temp_5') }
// QA r@109@01 :: ((r@109@01 in ns1@100@01) ==> (inv@110@01(r@109@01) == r@109@01))
fact { (all r_109_01: Ref | (set_in[r_109_01, ns1_100_01] => (Fun.inv_110_01[r_109_01] = r_109_01))) }
// (n2@103@01 in ns2@101@01)
fact { set_in[n2_103_01, ns2_101_01] }
// ($t@116@01 == Combine(_, $t@117@01))
one sig temp_6' in Snap {}
fact { combine[Unit, t_117_01, temp_6'] && 
       (t_116_01 = temp_6') }
// ($t@104@01 == Combine(_, $t@105@01))
one sig t_104_01 in Snap {}
one sig temp_7' in Snap {}
fact { combine[Unit, t_105_01, temp_7'] && 
       (t_104_01 = temp_7') }
// (n1@102@01 in ns1@100@01)
fact { set_in[n1_102_01, ns1_100_01] }
// ($t@108@01 == Combine(_, $t@111@01))
one sig temp_8' in Snap {}
fact { combine[Unit, t_111_01, temp_8'] && 
       (t_108_01 = temp_8') }
// ($t@120@01 == Combine(_, _))
one sig temp_9' in Snap {}
fact { combine[Unit, Unit, temp_9'] && 
       (t_120_01 = temp_9') }
// QA r@118@01 :: ((r@118@01 in ns2@101@01) ==> !((r@118@01 in ns1@100@01)))
fact { (all r_118_01: Ref | (set_in[r_118_01, ns2_101_01] => !(set_in[r_118_01, ns1_100_01]))) }
// QA r@109@01 :: ((r@109@01 in ns1@100@01) ==> !((r@109@01 == Null)))
fact { (all r_109_01: Ref | (set_in[r_109_01, ns1_100_01] => !((r_109_01 = NULL)))) }
// QA r@121@01 :: ((r@121@01 in ns2@101@01) ==> !((r@121@01 == Null)))
fact { (all r_121_01: Ref | (set_in[r_121_01, ns2_101_01] => !((r_121_01 = NULL)))) }

// Permission functions
one sig PermFun {
  next: (Ref -> FVF_Ref -> one Perm)
}

// Functions
one sig Fun {
  inv_110_01: (Ref -> lone Ref), 
  inv_122_01: (Ref -> lone Ref)
}

// Lookup functions
one sig Lookup {
  next: (FVF_Ref -> Ref -> lone Ref)
}
fact { all fvf: FVF_Ref, r: Ref | (one PermFun.next[r, fvf] and perm_less[Z, PermFun.next[r, fvf]]) => (Lookup.next[fvf, r] = r.next) }
// Other sorts
sig Set_Ref extends Set {} {
  set_elems in Ref
}
sig FVF_Ref {}

// No object unreachable from the Store
fact { Ref = Store.refTypedVars'.*refTypedFields' + NULL + (SortWrapper.wrapped <: Ref) + Store.ns1'.set_elems + Store.ns2'.set_elems }

// Signarure Restrictions
fact { Set = Store.ns1' + Store.ns2' }
fact { FVF_Ref = t_107_01 + t_119_01 + sm_114_01 }
fact { Snap = t_117_01 + temp_0' + temp_1' + t_120_01 + t_111_01 + temp_2' + t_115_01 + t_105_01 + temp_3' + temp_4' + t_108_01 + temp_5' + t_116_01 + temp_6' + t_104_01 + temp_7' + temp_8' + temp_9' + Unit }
fact { Perm = PermFun.next[Ref, FVF_Ref] + W + Z }
fact { Seq = none }
fact { Multiset = none }

run {} for 10 but 4 int, 2 Set, 3 FVF_Ref, 11 Snap, 3 Perm