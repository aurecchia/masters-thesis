// ===== Preamble (resources/preamble.als) =====
open util/boolean
open util/ternary
open util/integer
open util/relation
sig Snap {}
one sig Unit extends Snap {}
sig SortWrapper extends Snap {
    wrapped: one univ
}
pred sortwrapper_new [ e: univ, sw: Snap] {
    sw in SortWrapper
    sw.wrapped = e
}
abstract sig Combine extends Snap {
    left: one Snap,
    right: one Snap
}
pred combine [ l, r: Snap, c: Combine ] {
    c.left = l && c.right = r
    c.left != c && c.right != c
    c not in c.^left
	c not in c.^right
}
// ===== Perms (resources/perms_new.als) =====
abstract sig Perm {} 
one sig W in Perm {}
one sig Z in Perm {}
one sig PermRelations {
	eq: Perm -> Perm, 
	lessthan: Perm -> Perm,
	add: Perm -> Perm -> lone Perm,
	new: Int -> Int -> lone Perm
} {
	all a:Perm | perm_equals[a, a]
}
fact TransitiveLT {
	all a, b, c: Perm | perm_less[a, b] and perm_less[b, c] implies perm_less[a, c]
}
fact TransitiveEQ {
	all a, b, c: Perm | perm_equals[a, b] and perm_equals[b, c] implies perm_equals[a, c]
}
fact CommutativeEQ {
	all a, b: Perm | perm_equals[a, b] implies perm_equals[b, a]
}
fact { all a1, b1, a2, b2: Perm |
			( one PermRelations.add[a1, b1] and
			  one PermRelations.add[a2, b2] and
			  perm_equals[b1, b2] and
			  perm_equals[a1, a2] )
			=> perm_equals[PermRelations.add[a1, b1], PermRelations.add[a2, b2]] }
fact { perm_less[Z, W] }
fact { all p, p': Perm | perm_plus[ p, Z, p' ] => perm_equals[ p', p ] }
fact { all p, p': Perm | perm_plus[ Z, p, p' ] => perm_equals[ p', p ] }
pred perm_new[ n, d: Int, p': Perm ] {
	one PermRelations.new[n, d]
	p' = PermRelations.new[n, d]
	(n > d) => (perm_less[W, p'] and perm_less[Z, p'])
	(n = d) => (perm_equals[p', W])
	(n < d and n > 0) => (perm_less[p', W] and perm_less[Z, p'])
	(n = 0) => (perm_equals[p', Z])
}
pred perm_less[ p1, p2: Perm ] {
	one p1 and one p2
    ((p1 -> p2) in PermRelations.lessthan) and not perm_equals[p1, p2]
}
pred perm_at_most[ p1, p2: Perm ] {
	one p1 and one p2
    perm_less[p1, p2] or perm_equals[p1, p2]
}
pred perm_at_least[ p1, p2: Perm ] {
	one p1 and one p2
    perm_at_most[p2, p1]
}
pred perm_greater[ p1, p2: Perm ] {
	one p1 and one p2
    perm_less[p2, p1]
}
pred perm_plus[ p1, p2, p': Perm ] {
	one p1 and one p2 and one p'
	(perm_equals[p1, Z] iff perm_equals[p2, p'])
	(perm_equals[p2, Z] iff perm_equals[p1, p'])
	(perm_less[Z, p1] and perm_less[Z, p2] iff (
		perm_less[p1, p'] and perm_less[p2, p']
	))
	(p1 -> p2 -> p') in PermRelations.add
	(p2 -> p1 -> p') in PermRelations.add
	(perm_less[Z, p'] implies (perm_less[Z, p1] or perm_less[Z, p2]))
	(perm_less[Z, p2] implies perm_less[Z, p'])
	(perm_less[Z, p1] implies perm_less[Z, p'])
	(perm_less[Z, p1] iff perm_less[p2, p'])
	(perm_less[Z, p2] iff perm_less[p1, p'])
}
pred perm_minus[ p1, p2, p': Perm ] {
	one p1 and one p2 and one p'
	(perm_equals[p2, Z] iff perm_equals[p', p1])
	(perm_equals[p2, p1] iff perm_equals[p', Z])
	(perm_less[p2, p1] iff perm_less[Z, p'])
	(perm_less[Z, p2] iff perm_less[p', p1])
	perm_plus[ p', p2, p1 ]
	perm_plus[ p2, p', p1 ]
}
pred int_perm_div[ p: Perm, d: Int, p': Perm ] {
}
pred perm_mul[ p1, p2, p': Perm ] {
}
pred int_perm_mul[ i: Int, p, p': Perm ] {
}
pred perm_min[ p1, p2, p': Perm ] {
  one p1 and one p2 and one p'
  perm_less[p1, p2]
    => perm_equals[p1, p']
    else perm_equals[p2, p']
}
pred perm_equals [ p1, p2: Perm ] {
	one p1 and one p2
	(p1 -> p2) in PermRelations.eq
	(p2 -> p1) in PermRelations.eq
}
// ===== Sets (resources/set_fun.als) =====
abstract sig Set {
	set_elems: set univ
}
pred empty_set [ s': Set ] {
	no s'.set_elems
}
pred set_singleton [ e: univ, s': Set ] {
	s'.set_elems = e
	one e
}
pred set_add [ s1: Set, e: univ, s': Set ] {
	s'.set_elems = s1.set_elems + e
	one e
}
fun set_cardinality [ s1: Set ]: one Int {
	#(s1.set_elems)
}
pred set_difference [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems - s2.set_elems
}
pred set_intersection [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems & s2.set_elems
}
pred set_union [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems + s2.set_elems
}
pred set_in [ e: univ, s1: Set ] {
	e in s1.set_elems
	one e
	some s1.set_elems
}
pred set_subset [ s1, s2: Set ] {
	s1.set_elems in s2.set_elems
}
pred set_disjoint [ s1, s2: Set ] {
	disjoint[s1.set_elems, s2.set_elems]
}
pred set_equals [ s1, s2: Set ] {
	s1.set_elems = s2.set_elems
}
// ===== Seqs (resources/seq.als) =====
abstract sig Seq {
	// seq_rel: Int -> lone univ
	seq_rel: seq univ
} {
	isSeq[seq_rel]
}
pred seq_ranged [ from, to: Int, s': Seq ] {
	// { all i: Int | 0 <= i && i < sub[to, from] => s[i] = plus[from, i] }
	// #s = sub[to, from]
	s'.seq_rel = subseq[iden, from, sub[to, 1]]
}
pred seq_singleton [ e: univ, s': Seq ] {
	s'.seq_rel[0] = e
	#(s'.seq_rel) = 1
}
// NOTE: The sequence resulting from the wrapped 'append' operation may be
//		 truncated if the sequences are too long.
pred seq_append [ s1, s2, s': Seq ] {
	s'.seq_rel = append[s1.seq_rel, s2.seq_rel]
}
fun seq_length [ s: Seq ]: one Int {
	#(s.seq_rel)
}
fun seq_at [ s: Seq, i: Int ]: one univ {
	s.seq_rel[i]
}
pred seq_take [ s: Seq, i: Int, s': Seq ] {
	let to = sub[i, 1] |
	s'.seq_rel = subseq[ s.seq_rel, 0, to]
}
pred seq_drop [ s: Seq, i: Int, s': Seq ] {
	let to = sub[#s.seq_rel, 1] |
	s'.seq_rel = subseq[ s.seq_rel, i, to ]
}
pred seq_in [ s1: Seq, e: univ ] {
	e in elems[s1.seq_rel]
}
pred seq_update [ s: Seq, i: Int, e: univ, s': Seq ] {
	s'.seq_rel = setAt[s.seq_rel, i, e]
} 
// ===== Multiset (resources/multiset.als) =====
abstract sig Multiset {
	ms_elems: univ -> lone Int
} {
	all i: univ.ms_elems | gt[i, 0]
}
pred empty_multiset [ ms': Multiset ] {
    no ms'.ms_elems
}
pred multiset_singleton [ e: univ, ms': Multiset ] {
	ms'.ms_elems = (e -> 1)
}
pred multiset_add [ ms1: Multiset, elem: univ, ms': Multiset ] {
	ms'.ms_elems =	{ e: univ, v: Int | e in (elem - dom[ms1.ms_elems]) and v = 1 } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] - elem) and v = ms1.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] & elem) and v = add[ms1.ms_elems[e], 1] }
}
fun multiset_cardinality_fun [ ms: Multiset ]: one Int {
    sum ms.ms_elems[univ]
}
pred multiset_cardinality [ ms: Multiset, card: Int ] {
	card = (let s = { c: Int, e: univ | (e -> c) in ms.ms_elems } |
					(sum i: (s).univ | mul[#(s[i]), i]) )
	card >= 0
}
pred multiset_difference [ ms1, ms2, ms': Multiset ] {
	ms'.ms_elems = { e: univ, v: Int | e in (dom[ms1.ms_elems] - dom[ms2.ms_elems]) and v = ms1.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and
									e.(ms2.ms_elems) < e.(ms1.ms_elems) and
									v = minus[e.(ms1.ms_elems), e.(ms2.ms_elems)] }
}
pred multiset_intersection [ ms1, ms2, ms': Multiset ] {
	ms'.ms_elems = { e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and v = min[e.(ms1.ms_elems) + e.(ms2.ms_elems)] }
}
pred multiset_union [ ms1, ms2, ms': Multiset ] {
	ms'.ms_elems = { e: univ, v: Int | e in (dom[ms2.ms_elems] - dom[ms1.ms_elems]) and v = ms2.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] - dom[ms2.ms_elems]) and v = ms1.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and v = add[ms1.ms_elems[e], ms2.ms_elems[e]] }
}
pred multiset_subset [ ms1, ms2: Multiset ] {
	dom[ms1.ms_elems] in dom[ms2.ms_elems]
	{ all e: dom[ms1.ms_elems] | ms1.ms_elems[e] <= ms2.ms_elems[e] }
}
pred multiset_count [ ms1: Multiset, e: univ, c: Int ] {
	c = ms1.ms_elems[e]
} 
fun multiset_count_fun [ ms1: Multiset, e: univ ]: one Int {
	ms1.ms_elems[e]
} 
pred multiset_equals [ ms1, ms2: Multiset ] {
    ms1.ms_elems = ms2.ms_elems
}
sig Ref {
  val: lone Int, 
  refTypedFields': set Ref
} {
  refTypedFields' = none
}

one sig NULL extends Ref {}
fact { NULL.refTypedFields' = none && no NULL.val }

one sig Store {
  ns': one Set_Ref, 
  refTypedVars': set Ref
} {
  refTypedVars' = none
}
one sig ns_1_01 in Set_Ref {}
fact { Store.ns' = ns_1_01 }

// Heap Chunks
// QA r@5@01 :: ((r@5@01 in ns@1@01) ==> (inv@6@01(r@5@01) == r@5@01))
fact { (all r_5_01: Ref | (set_in[r_5_01, ns_1_01] => (Fun.inv_6_01[r_5_01] = r_5_01))) }
// QA r :: ((inv@6@01(r) in ns@1@01) ==> (inv@6@01(r) == r))
fact { (all r: Ref | (set_in[Fun.inv_6_01[r], ns_1_01] => (Fun.inv_6_01[r] = r))) }
one sig t_3_01 in FVF_Int {}
fact { all r: Ref | (set_in[Fun.inv_6_01[r], ns_1_01] implies W else Z) = PermFun.val[r, t_3_01] }
fact { all r: Ref | (some fvf: (t_3_01) | one PermFun.val[r, fvf] and perm_less[Z, PermFun.val[r, fvf]]) <=> (one r.val) }
fact { all r: Ref, fvf: (t_3_01) | one PermFun.val[r, fvf] => (perm_at_most[PermFun.val[r, fvf], W]) }
one sig Preds {}

// Path Conditions
// QA r@5@01 :: ((r@5@01 in ns@1@01) ==> !((r@5@01 == Null)))
fact { (all r_5_01: Ref | (set_in[r_5_01, ns_1_01] => !((r_5_01 = NULL)))) }
// QA r@7@01 :: ((r@7@01 in ns@1@01) ==> QE r2@8@01 :: !((r2@8@01 == r@7@01)) && (!((r2@8@01 == r@7@01)) ==> (r2@8@01 in ns@1@01) && ((r2@8@01 in ns@1@01) ==> (Lookup(val, sm@11@01(), r@7@01) == Lookup(val, sm@11@01(), r2@8@01)))))
one sig sm_11_01 in FVF_Int {}
fact { (all r_7_01: Ref | (set_in[r_7_01, ns_1_01] => (some r2_8_01: Ref | (!((r2_8_01 = r_7_01)) && (!((r2_8_01 = r_7_01)) => (set_in[r2_8_01, ns_1_01] && (set_in[r2_8_01, ns_1_01] => (Lookup.val[sm_11_01, r_7_01] = Lookup.val[sm_11_01, r2_8_01])))))))) }
// QA r :: ((inv@6@01(r) in ns@1@01) ==> (inv@6@01(r) == r))
fact { (all r: Ref | (set_in[Fun.inv_6_01[r], ns_1_01] => (Fun.inv_6_01[r] = r))) }
// QA r :: ((inv@6@01(r) in ns@1@01) ==> (Lookup(val, sm@11@01(), r) == Lookup(val, $t@3@01, r)))
fact { (all r: Ref | (set_in[Fun.inv_6_01[r], ns_1_01] => (Lookup.val[sm_11_01, r] = Lookup.val[t_3_01, r]))) }
// (SetCardinality:(ns@1@01) == 5)
fact { (set_cardinality[ns_1_01] = 5) }
// ($t@4@01 == Combine(_, _))
one sig t_4_01 in Snap {}
one sig temp_0' in Snap {}
fact { combine[Unit, Unit, temp_0'] && 
       (t_4_01 = temp_0') }
// QA r@5@01 :: ((r@5@01 in ns@1@01) ==> (inv@6@01(r@5@01) == r@5@01))
fact { (all r_5_01: Ref | (set_in[r_5_01, ns_1_01] => (Fun.inv_6_01[r_5_01] = r_5_01))) }
// ($t@2@01 == Combine(SortWrapper($t@3@01, Snap), $t@4@01))
one sig t_2_01 in Snap {}
one sig temp_1' in Snap {}
one sig temp_2' in Snap {}
fact { sortwrapper_new[t_3_01, temp_2'] && 
       combine[temp_2', t_4_01, temp_1'] && 
       (t_2_01 = temp_1') }

// Permission functions
one sig PermFun {
  val: (Ref -> FVF_Int -> lone Perm)
}

// Functions
one sig Fun {
  inv_6_01: (Ref -> lone Ref)
}

// Lookup functions
one sig Lookup {
  val: (FVF_Int -> Ref -> lone Int)
}
fact { all fvf: FVF_Int, r: Ref | (one PermFun.val[r, fvf] and perm_less[Z, PermFun.val[r, fvf]]) => (Lookup.val[fvf, r] = r.val) }
// Other sorts
sig Set_Ref extends Set {} {
  set_elems in Ref
}
sig FVF_Int {}

// No object unreachable from the Store
fact { Ref = Store.refTypedVars'.*refTypedFields' + NULL + (SortWrapper.wrapped <: Ref) + Store.ns'.set_elems }

// Signarure Restrictions
fact { Set = Store.ns' }
fact { FVF_Int = t_3_01 + sm_11_01 }
fact { Snap = t_4_01 + temp_0' + t_2_01 + temp_1' + temp_2' + Unit }
fact { Perm = PermFun.val[Ref, FVF_Int] + W + Z }
fact { Seq = none }
fact { Multiset = none }

run {} for 10 but 5 int, 1 Set, 2 FVF_Int, 6 Snap, 3 Perm
