// ===== Preamble (resources/preamble.als) =====
open util/boolean
open util/ternary
open util/integer
open util/relation
abstract sig Snap {}
one sig Unit extends Snap {}
abstract sig SortWrapper extends Snap {
    wrapped: one univ
}
pred sortwrapper_new [ e: univ, sw: Snap] {
    sw in SortWrapper
    sw.wrapped = e
}
abstract sig Combine extends Snap {
    left: one Snap,
    right: one Snap
}
pred combine [ l, r: Snap, c: Combine ] {
    c.left = l && c.right = r
    c.left != c && c.right != c
    c not in c.^left
	c not in c.^right
}
abstract sig CustomInt {
    value: one Int
}
fact { all i1, i2: CustomInt | i1 = i2 <=> i1.value = i2.value }
// ===== Perms (resources/perms.als) =====
abstract sig Perm {
    num: one Int,
    denom: one Int
} {
    num >= 0
    denom > 0
}
one sig W in Perm {} {
    num = 1
    denom = 1
}
one sig Z in Perm {} {
    num = 0
    denom = 1
}
pred perm_new[ n, d: Int, p': Perm ] {
    p'.num = n
    p'.denom = d
}
pred perm_less[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] < mul[p2.num, p1.denom]
}
pred perm_at_most[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] <= mul[p2.num, p1.denom]
}
pred perm_at_least[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] >= mul[p2.num, p1.denom]
}
pred perm_greater[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] > mul[p2.num, p1.denom]
}
pred perm_plus[ p1, p2, p': Perm ] {
  (p1.denom = p2.denom)
    =>
        (p'.num = plus[p1.num, p2.num] &&
         p'.denom = p1.denom)
    else
        (p'.num = plus[mul[p1.num, p2.denom], mul[p2.num, p1.denom]] &&
         p'.denom = mul[p1.denom, p2.denom])
}
pred perm_minus[ p1, p2, p': Perm ] {
	perm_equals[ p1, p2 ]
		=> p' = Z
		else (
      p1.denom = p2.denom
        => (
          p'.num = minus[p1.num, p2.num] and
          p'.denom = p1.denom
        ) else (
          p'.num = minus[mul[p1.num, p2.denom], mul[p2.num, p1.denom]] and
          p'.denom = mul[p1.denom, p2.denom]
        )
		)
}
pred int_perm_div[ p: Perm, d: Int, p': Perm ] {
  p'.num = p.num
  p'.denom = mul[p.denom, d]
}
pred perm_mul[ p1, p2, p': Perm ] {
  p'.num = mul[p1.num, p2.num]
  p'.denom = mul[p1.denom, p2.denom]
}
pred int_perm_mul[ i: Int, p, p': Perm ] {
  p'.num = mul[p.num, i]
  p'.denom = p.denom
}
pred perm_min[ p1, p2, p': Perm ] {
  mul[p1.num, p2.denom] < mul[p2.num, p1.denom]
    => (p' = p1)
    else (p' = p2)
}
pred perm_equals [ p1, p2: Perm ] {
	// p1 = p2
  // The additional two clauses create problems with quantified permissions
  (p1.num = p2.num && p1.denom = p2.denom)
  || (mul[p1.num, p2.denom] = mul[p1.denom, p2.num])
}
// ===== Sets (resources/set_fun.als) =====
abstract sig Set {
	set_elems: set univ
}
pred empty_set [ s': Set ] {
	no s'.set_elems
}
pred set_singleton [ e: univ, s': Set ] {
	s'.set_elems = e
	one e
}
pred set_add [ s1: Set, e: univ, s': Set ] {
	s'.set_elems = s1.set_elems + e
	one e
}
pred set_cardinality [ s1: Set, i: CustomInt ] {
	#(s1.set_elems) = i.value
}
pred set_difference [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems - s2.set_elems
}
pred set_intersection [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems & s2.set_elems
}
pred set_union [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems + s2.set_elems
}
pred set_in [ e: univ, s1: Set ] {
	e in s1.set_elems
	one e
	some s1.set_elems
}
pred set_subset [ s1, s2: Set ] {
	s1.set_elems in s2.set_elems
}
pred set_disjoint [ s1, s2: Set ] {
	disjoint[s1.set_elems, s2.set_elems]
}
pred set_equals [ s1, s2: Set ] {
	s1.set_elems = s2.set_elems
}
// ===== Seqs (resources/seq.als) =====
abstract sig Seq {
	seq_rel: seq univ
} {
	isSeq[seq_rel]
}
pred seq_ranged [ from, to: CustomInt, s': Seq ] {
	let ints = { i: Int, ci: CustomInt | ci.value = i and from.value <= i and i < to.value } |
	#ints = sub[to.value, from.value] and
	s'.seq_rel = subseq[ints, from.value, sub[to.value, 1]]
}
pred seq_singleton [ e: univ, s': Seq ] {
	s'.seq_rel[0] = e
	#(s'.seq_rel) = 1
}
// NOTE: The sequence resulting from the wrapped 'append' operation may be
//		 truncated if the sequences are too long.
pred seq_append [ s1, s2, s': Seq ] {
	s'.seq_rel = append[s1.seq_rel, s2.seq_rel]
}
pred seq_length [ s: Seq, i: CustomInt ] {
	#(s.seq_rel) = i.value
}
fun seq_at [ s: Seq, i: CustomInt ]: one univ {
    s.seq_rel[i.value]
}
pred seq_take [ s: Seq, i: CustomInt, s': Seq ] {
	let to = sub[i.value, 1] |
	s'.seq_rel = subseq[ s.seq_rel, 0, to]
}
pred seq_drop [ s: Seq, i: CustomInt, s': Seq ] {
	let to = sub[#s.seq_rel, 1] |
	s'.seq_rel = subseq[ s.seq_rel, i.value, to ]
}
pred seq_in [ s1: Seq, e: univ ] {
	e in elems[s1.seq_rel]
}
pred seq_update [ s: Seq, i: CustomInt, e: univ, s': Seq ] {
	s'.seq_rel = setAt[s.seq_rel, i.value, e]
} 
// ===== Multiset (resources/multiset.als) =====
abstract sig Multiset {
    ms_elems: univ -> lone Int
} {
    all i: univ.ms_elems | gt[i, 0]
}
pred empty_multiset [ ms': Multiset ] {
    no ms'.ms_elems
}
pred multiset_singleton [ e: univ, ms': Multiset ] {
    ms'.ms_elems = (e -> 1)
}
pred multiset_add [ ms1: Multiset, elem: univ, ms': Multiset ] {
    ms'.ms_elems =    { e: univ, v: Int | e in (elem - dom[ms1.ms_elems]) and v = 1 } +
                { e: univ, v: Int | e in (dom[ms1.ms_elems] - elem) and v = ms1.ms_elems[e] } +
                { e: univ, v: Int | e in (dom[ms1.ms_elems] & elem) and v = add[ms1.ms_elems[e], 1] }
}
pred multiset_cardinality [ ms: Multiset, card: CustomInt ] {
    card.value = (let s = { c: Int, e: univ | (e -> c) in ms.ms_elems } |
                    (sum i: (s).univ | mul[#(s[i]), i]) )
    card.value >= 0
}
pred multiset_difference [ ms1, ms2, ms': Multiset ] {
    ms'.ms_elems = { e: univ, v: Int | e in (dom[ms1.ms_elems] - dom[ms2.ms_elems]) and v = ms1.ms_elems[e] } +
                { e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and
                                    e.(ms2.ms_elems) < e.(ms1.ms_elems) and
                                    v = minus[e.(ms1.ms_elems), e.(ms2.ms_elems)] }
}
pred multiset_intersection [ ms1, ms2, ms': Multiset ] {
    ms'.ms_elems = { e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and v = min[e.(ms1.ms_elems) + e.(ms2.ms_elems)] }
}
pred multiset_union [ ms1, ms2, ms': Multiset ] {
    ms'.ms_elems = { e: univ, v: Int | e in (dom[ms2.ms_elems] - dom[ms1.ms_elems]) and v = ms2.ms_elems[e] } +
                { e: univ, v: Int | e in (dom[ms1.ms_elems] - dom[ms2.ms_elems]) and v = ms1.ms_elems[e] } +
                { e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and v = add[ms1.ms_elems[e], ms2.ms_elems[e]] }
}
pred multiset_subset [ ms1, ms2: Multiset ] {
    dom[ms1.ms_elems] in dom[ms2.ms_elems]
    { all e: dom[ms1.ms_elems] | ms1.ms_elems[e] <= ms2.ms_elems[e] }
}
pred multiset_count [ ms1: Multiset, e: univ, c: CustomInt ] {
    c.value = ms1.ms_elems[e]
} 
pred multiset_equals [ ms1, ms2: Multiset ] {
    ms1.ms_elems = ms2.ms_elems
}
sig Ref {
  val: lone CustomInt, 
  refTypedFields': set Ref
} {
  refTypedFields' = none
}

one sig NULL extends Ref {}
fact { NULL.refTypedFields' = none && no NULL.val }

one sig Store {
  ns': one Set_Ref, 
  n1': one Ref, 
  p1': one Perm, 
  p2': one Perm, 
  p3': one Perm, 
  p4': one Perm, 
  p5': one Perm, 
  refTypedVars': set Ref
} {
  refTypedVars' = n1'
}
one sig ns_65_01 in Set_Ref {}
one sig n1_66_01 in Ref {}
one sig p1_72_01 in Perm {}
one sig p2_73_01 in Perm {}
one sig p3_74_01 in Perm {}
one sig p4_75_01 in Perm {}
one sig p5_76_01 in Perm {}
fact { Store.ns' = ns_65_01 }
fact { Store.n1' = n1_66_01 }
fact { Store.p1' = p1_72_01 }
fact { Store.p2' = p2_73_01 }
fact { Store.p3' = p3_74_01 }
fact { Store.p4' = p4_75_01 }
fact { Store.p5' = p5_76_01 }

// Heap Chunks
// QA r@70@01 :: ((r@70@01 in ns@65@01) ==> (inv@71@01(r@70@01) == r@70@01))
fact { (all r_70_01: Ref | (set_in[r_70_01, ns_65_01] => (Fun.inv_71_01[r_70_01] = r_70_01))) }
// QA r :: ((inv@71@01(r) in ns@65@01) ==> (inv@71@01(r) == r))
fact { (all r: Ref | (set_in[Fun.inv_71_01[r], ns_65_01] => (Fun.inv_71_01[r] = r))) }
one sig t_68_01 in FVF_CustomInt {}
one sig fresh_quantifier_vars_0 {
  q_temp_0': Ref -> lone Perm, 
  q_temp_2': Ref -> lone Perm, 
  q_temp_3': Ref -> lone Perm, 
  q_temp_4': Ref -> lone Perm
}
fact { all r: Ref | perm_minus[(set_in[Fun.inv_71_01[r], ns_65_01] implies W else Z), PTAKEN.pTaken_82_01[r], fresh_quantifier_vars_0.q_temp_4'[r]] && 
       perm_minus[fresh_quantifier_vars_0.q_temp_4'[r], PTAKEN.pTaken_84_01[r], fresh_quantifier_vars_0.q_temp_3'[r]] && 
       perm_minus[fresh_quantifier_vars_0.q_temp_3'[r], PTAKEN.pTaken_86_01[r], fresh_quantifier_vars_0.q_temp_2'[r]] && 
       perm_minus[fresh_quantifier_vars_0.q_temp_2'[r], PTAKEN.pTaken_88_01[r], fresh_quantifier_vars_0.q_temp_0'[r]] && 
       fresh_quantifier_vars_0.q_temp_0'[r] = PermFun.val[r, t_68_01] }
fact { all r: Ref | (some fvf: (t_68_01) | one PermFun.val[r, fvf] and perm_less[Z, PermFun.val[r, fvf]]) <=> (one r.val) }
fact { all r: Ref, fvf: (t_68_01) | one PermFun.val[r, fvf] => (perm_at_most[PermFun.val[r, fvf], W]) }
one sig Preds {}

// Path Conditions
// ($t@81@01 == Combine(_, _))
one sig t_81_01 in Snap {}
one sig temp_0' in Snap {}
fact { combine[Unit, Unit, temp_0'] }
fact { (t_81_01 = temp_0') }
// (p1@72@01 > Z)
fact { perm_greater[p1_72_01, Z] }
// (p4@75@01 > Z)
fact { perm_greater[p4_75_01, Z] }
// ($t@80@01 == Combine(_, $t@81@01))
one sig t_80_01 in Snap {}
one sig temp_1' in Snap {}
fact { combine[Unit, t_81_01, temp_1'] }
fact { (t_80_01 = temp_1') }
// ($t@78@01 == Combine(_, $t@79@01))
one sig t_78_01 in Snap {}
one sig temp_2' in Snap {}
one sig t_79_01 in Snap {}
fact { combine[Unit, t_79_01, temp_2'] }
fact { (t_78_01 = temp_2') }
// (((((p1@72@01 + p2@73@01) + p3@74@01) + p4@75@01) + p5@76@01) == W)
one sig temp_3' in Perm {}
one sig temp_4' in Perm {}
one sig temp_5' in Perm {}
one sig temp_6' in Perm {}
fact { perm_plus[p1_72_01, p2_73_01, temp_6'] }
fact { perm_plus[temp_6', p3_74_01, temp_5'] }
fact { perm_plus[temp_5', p4_75_01, temp_4'] }
fact { perm_plus[temp_4', p5_76_01, temp_3'] }
fact { perm_equals[temp_3', W] }
// ($t@67@01 == Combine(SortWrapper($t@68@01, Snap), $t@69@01))
one sig t_67_01 in Snap {}
one sig temp_7' in Snap {}
one sig temp_8' in Snap {}
one sig t_69_01 in Snap {}
fact { sortwrapper_new[t_68_01, temp_8'] }
fact { combine[temp_8', t_69_01, temp_7'] }
fact { (t_67_01 = temp_7') }
// ((Z < ((inv@71@01(n1@66@01) in ns@65@01) ? W : Z - pTaken@82@01(n1@66@01))) ==> (Lookup(val, sm@85@01(), n1@66@01) == Lookup(val, $t@68@01, n1@66@01)))
one sig temp_9' in Perm {}
one sig sm_85_01 in FVF_CustomInt {}
fact { perm_minus[(set_in[Fun.inv_71_01[n1_66_01], ns_65_01] implies W else Z), PTAKEN.pTaken_82_01[n1_66_01], temp_9'] }
fact { (perm_less[Z, temp_9'] => (Lookup.val[sm_85_01, n1_66_01].value = Lookup.val[t_68_01, n1_66_01].value)) }
// (p3@74@01 > Z)
fact { perm_greater[p3_74_01, Z] }
// ((Z < ((((inv@71@01(n1@66@01) in ns@65@01) ? W : Z - pTaken@82@01(n1@66@01)) - pTaken@84@01(n1@66@01)) - pTaken@86@01(n1@66@01))) ==> (Lookup(val, sm@89@01(), n1@66@01) == Lookup(val, $t@68@01, n1@66@01)))
one sig temp_10' in Perm {}
one sig temp_11' in Perm {}
one sig temp_12' in Perm {}
one sig sm_89_01 in FVF_CustomInt {}
fact { perm_minus[(set_in[Fun.inv_71_01[n1_66_01], ns_65_01] implies W else Z), PTAKEN.pTaken_82_01[n1_66_01], temp_12'] }
fact { perm_minus[temp_12', PTAKEN.pTaken_84_01[n1_66_01], temp_11'] }
fact { perm_minus[temp_11', PTAKEN.pTaken_86_01[n1_66_01], temp_10'] }
fact { (perm_less[Z, temp_10'] => (Lookup.val[sm_89_01, n1_66_01].value = Lookup.val[t_68_01, n1_66_01].value)) }
// ((Z < (((inv@71@01(n1@66@01) in ns@65@01) ? W : Z - pTaken@82@01(n1@66@01)) - pTaken@84@01(n1@66@01))) ==> (Lookup(val, sm@87@01(), n1@66@01) == Lookup(val, $t@68@01, n1@66@01)))
one sig temp_13' in Perm {}
one sig temp_14' in Perm {}
one sig sm_87_01 in FVF_CustomInt {}
fact { perm_minus[(set_in[Fun.inv_71_01[n1_66_01], ns_65_01] implies W else Z), PTAKEN.pTaken_82_01[n1_66_01], temp_14'] }
fact { perm_minus[temp_14', PTAKEN.pTaken_84_01[n1_66_01], temp_13'] }
fact { (perm_less[Z, temp_13'] => (Lookup.val[sm_87_01, n1_66_01].value = Lookup.val[t_68_01, n1_66_01].value)) }
// QA r@70@01 :: ((r@70@01 in ns@65@01) ==> !((r@70@01 == Null)))
fact { (all r_70_01: Ref | (set_in[r_70_01, ns_65_01] => !((r_70_01 = NULL)))) }
// (p5@76@01 > Z)
fact { perm_greater[p5_76_01, Z] }
// (SetCardinality:(ns@65@01) == 4)
one sig temp_15' in CustomInt {}
one sig temp_16' in CustomInt {}
fact { set_cardinality[ns_65_01, temp_15'] }
fact { temp_16'.value = 4 }
fact { (temp_15'.value = temp_16'.value) }
// ($t@77@01 == _)
one sig t_77_01 in Snap {}
fact { (t_77_01 = Unit) }
// ((inv@71@01(n1@66@01) in ns@65@01) ==> (Lookup(val, sm@83@01(), n1@66@01) == Lookup(val, $t@68@01, n1@66@01)))
one sig sm_83_01 in FVF_CustomInt {}
fact { (set_in[Fun.inv_71_01[n1_66_01], ns_65_01] => (Lookup.val[sm_83_01, n1_66_01].value = Lookup.val[t_68_01, n1_66_01].value)) }
// QA r :: ((inv@71@01(r) in ns@65@01) ==> (inv@71@01(r) == r))
fact { (all r: Ref | (set_in[Fun.inv_71_01[r], ns_65_01] => (Fun.inv_71_01[r] = r))) }
// ($t@69@01 == Combine(_, _))
one sig temp_17' in Snap {}
fact { combine[Unit, Unit, temp_17'] }
fact { (t_69_01 = temp_17') }
// QA r@70@01 :: ((r@70@01 in ns@65@01) ==> (inv@71@01(r@70@01) == r@70@01))
fact { (all r_70_01: Ref | (set_in[r_70_01, ns_65_01] => (Fun.inv_71_01[r_70_01] = r_70_01))) }
// (n1@66@01 in ns@65@01)
fact { set_in[n1_66_01, ns_65_01] }
// (p2@73@01 > Z)
fact { perm_greater[p2_73_01, Z] }
// ($t@79@01 == Combine(_, $t@80@01))
one sig temp_18' in Snap {}
fact { combine[Unit, t_80_01, temp_18'] }
fact { (t_79_01 = temp_18') }

// Permission functions
one sig PermFun {
  val: (Ref -> FVF_CustomInt -> lone Perm)
}

// Functions
one sig Fun {
  inv_71_01: (Ref -> one Ref)
}

// !!! Non-translated fact: 
//     Could not translate quantified variables: QA s@$, r@0@00 :: let result@1@00 = hdf2%limited(s@$, r@0@00) in (result@1@00 == SortWrapper(First:(s@$), Int))

// Lookup functions
one sig Lookup {
  val: (FVF_CustomInt -> Ref -> one CustomInt)
}
fact { all fvf: FVF_CustomInt, r: Ref | (one PermFun.val[r, fvf] and perm_less[Z, PermFun.val[r, fvf]]) => (Lookup.val[fvf, r] = r.val) }
// Other sorts
sig Set_Ref extends Set {} {
  set_elems in Ref
}
sig FVF_CustomInt {}

// Macros
// QA r :: (pTaken@82@01(r) == (r == n1@66@01) ? ((inv@71@01(r) in ns@65@01) ? W : Z PermMin p1@72@01) : Z)
one sig fresh_quantifier_vars_1 {
  q_temp_0': Ref -> lone Perm
}
fact { (all r: Ref | perm_min[(set_in[Fun.inv_71_01[r], ns_65_01] implies W else Z), p1_72_01, fresh_quantifier_vars_1.q_temp_0'[r]]) }
fact { (all r: Ref | perm_equals[PTAKEN.pTaken_82_01[r], ((r = n1_66_01) implies fresh_quantifier_vars_1.q_temp_0'[r] else Z)]) }
// QA r :: (pTaken@84@01(r) == (r == n1@66@01) ? (((inv@71@01(r) in ns@65@01) ? W : Z - pTaken@82@01(r)) PermMin p2@73@01) : Z)
one sig fresh_quantifier_vars_2 {
  q_temp_0': Ref -> lone Perm, 
  q_temp_2': Ref -> lone Perm
}
fact { (all r: Ref | perm_minus[(set_in[Fun.inv_71_01[r], ns_65_01] implies W else Z), PTAKEN.pTaken_82_01[r], fresh_quantifier_vars_2.q_temp_2'[r]]) }
fact { (all r: Ref | perm_min[fresh_quantifier_vars_2.q_temp_2'[r], p2_73_01, fresh_quantifier_vars_2.q_temp_0'[r]]) }
fact { (all r: Ref | perm_equals[PTAKEN.pTaken_84_01[r], ((r = n1_66_01) implies fresh_quantifier_vars_2.q_temp_0'[r] else Z)]) }
// QA r :: (pTaken@86@01(r) == (r == n1@66@01) ? ((((inv@71@01(r) in ns@65@01) ? W : Z - pTaken@82@01(r)) - pTaken@84@01(r)) PermMin p3@74@01) : Z)
one sig fresh_quantifier_vars_3 {
  q_temp_0': Ref -> lone Perm, 
  q_temp_2': Ref -> lone Perm, 
  q_temp_3': Ref -> lone Perm
}
fact { (all r: Ref | perm_minus[(set_in[Fun.inv_71_01[r], ns_65_01] implies W else Z), PTAKEN.pTaken_82_01[r], fresh_quantifier_vars_3.q_temp_3'[r]]) }
fact { (all r: Ref | perm_minus[fresh_quantifier_vars_3.q_temp_3'[r], PTAKEN.pTaken_84_01[r], fresh_quantifier_vars_3.q_temp_2'[r]]) }
fact { (all r: Ref | perm_min[fresh_quantifier_vars_3.q_temp_2'[r], p3_74_01, fresh_quantifier_vars_3.q_temp_0'[r]]) }
fact { (all r: Ref | perm_equals[PTAKEN.pTaken_86_01[r], ((r = n1_66_01) implies fresh_quantifier_vars_3.q_temp_0'[r] else Z)]) }
// QA r :: (pTaken@88@01(r) == (r == n1@66@01) ? (((((inv@71@01(r) in ns@65@01) ? W : Z - pTaken@82@01(r)) - pTaken@84@01(r)) - pTaken@86@01(r)) PermMin p4@75@01) : Z)
one sig fresh_quantifier_vars_4 {
  q_temp_0': Ref -> lone Perm, 
  q_temp_2': Ref -> lone Perm, 
  q_temp_3': Ref -> lone Perm, 
  q_temp_4': Ref -> lone Perm
}
fact { (all r: Ref | perm_minus[(set_in[Fun.inv_71_01[r], ns_65_01] implies W else Z), PTAKEN.pTaken_82_01[r], fresh_quantifier_vars_4.q_temp_4'[r]]) }
fact { (all r: Ref | perm_minus[fresh_quantifier_vars_4.q_temp_4'[r], PTAKEN.pTaken_84_01[r], fresh_quantifier_vars_4.q_temp_3'[r]]) }
fact { (all r: Ref | perm_minus[fresh_quantifier_vars_4.q_temp_3'[r], PTAKEN.pTaken_86_01[r], fresh_quantifier_vars_4.q_temp_2'[r]]) }
fact { (all r: Ref | perm_min[fresh_quantifier_vars_4.q_temp_2'[r], p4_75_01, fresh_quantifier_vars_4.q_temp_0'[r]]) }
fact { (all r: Ref | perm_equals[PTAKEN.pTaken_88_01[r], ((r = n1_66_01) implies fresh_quantifier_vars_4.q_temp_0'[r] else Z)]) }
one sig PTAKEN {
  pTaken_82_01: Ref -> one Perm, 
  pTaken_84_01: Ref -> one Perm, 
  pTaken_86_01: Ref -> one Perm, 
  pTaken_88_01: Ref -> one Perm
}

// No object unreachable from the Store
fact { Ref = Store.refTypedVars'.*refTypedFields' + NULL + (SortWrapper.wrapped <: Ref) + Store.ns'.set_elems }

// Signarure Restrictions
fact { Set = Store.ns' }
fact { Perm = Store.p1' + Store.p2' + Store.p3' + Store.p4' + Store.p5' + temp_3' + temp_4' + temp_5' + temp_6' + temp_9' + temp_10' + temp_11' + temp_12' + temp_13' + temp_14' + PermFun.val[Ref, FVF_CustomInt] + W + Z }
fact { FVF_CustomInt = t_68_01 + sm_85_01 + sm_89_01 + sm_87_01 + sm_83_01 }
fact { Snap = t_81_01 + temp_0' + t_80_01 + temp_1' + t_78_01 + temp_2' + t_79_01 + t_67_01 + temp_7' + temp_8' + t_69_01 + t_77_01 + temp_17' + temp_18' + Unit }
fact { Seq = none }
fact { Multiset = none }

run {} for 11 but 4 int, 1 Set, 11 Perm, 5 FVF_CustomInt