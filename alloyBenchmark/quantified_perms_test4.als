// ===== Preamble (resources/preamble.als) =====
open util/boolean
open util/ternary
open util/integer
open util/relation
sig Snap {}
one sig Unit extends Snap {}
sig SortWrapper extends Snap {
    wrapped: one univ
}
pred sortwrapper_new [ e: univ, sw: Snap] {
    sw in SortWrapper
    sw.wrapped = e
}
abstract sig Combine extends Snap {
    left: one Snap,
    right: one Snap
}
pred combine [ l, r: Snap, c: Combine ] {
    c.left = l && c.right = r
}
// ===== Perms (resources/perms.als) =====
abstract sig Perm {
    num: one Int,
    denom: one Int
} {
    num >= 0
    denom > 0
    // Silicon does not define this in SMT and defining it here would prevent us
    // from having sums of permissions that exceed W
    // num <= denom
}
one sig W in Perm {} {
    num = 1
    denom = 1
}
one sig Z in Perm {} {
    num = 0
    denom = 1
}
pred perm_new[ n, d: Int, p': Perm ] {
    p'.num = n
    p'.denom = d
}
pred perm_less[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] < mul[p2.num, p1.denom]
}
pred perm_at_most[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] <= mul[p2.num, p1.denom]
}
pred perm_at_least[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] >= mul[p2.num, p1.denom]
}
pred perm_greater[ p1, p2: Perm ] {
  mul[p1.num, p2.denom] > mul[p2.num, p1.denom]
}
pred perm_plus[ p1, p2, p': Perm ] {
  (p1.denom = p2.denom)
    =>
        (p'.num = plus[p1.num, p2.num] &&
         p'.denom = p1.denom)
    else
        (p'.num = plus[mul[p1.num, p2.denom], mul[p2.num, p1.denom]] &&
         p'.denom = mul[p1.denom, p2.denom])
}
pred perm_minus[ p1, p2, p': Perm ] {
	perm_equals[ p1, p2 ]
		=> p' = Z
		else (
		  p'.num = minus[mul[p1.num, p2.denom], mul[p2.num, p1.denom]] and
		  p'.denom = mul[p1.denom, p2.denom]
		)
}
pred int_perm_div[ p: Perm, d: Int, p': Perm ] {
  p'.num = p.num
  p'.denom = mul[p.denom, d]
}
pred perm_mul[ p1, p2, p': Perm ] {
  p'.num = mul[p1.num, p2.num]
  p'.denom = mul[p1.denom, p2.denom]
}
pred int_perm_mul[ i: Int, p, p': Perm ] {
  p'.num = mul[p.num, i]
  p'.denom = p.denom
}
pred perm_min[ p1, p2, p': Perm ] {
  mul[p1.num, p2.denom] < mul[p2.num, p1.denom]
    => (p' = p1)
    else (p' = p2)
}
pred perm_equals [ p1, p2: Perm ] {
	// p1 = p2
  // The additional two clauses create problems with quantified permissions
  (p1.num = p2.num && p1.denom = p2.denom)
  || (mul[p1.num, p2.denom] = mul[p1.denom, p2.num])
}
// ===== Sets (resources/set_fun.als) =====
abstract sig Set {
	set_elems: set univ
}
pred empty_set [ s': Set ] {
	no s'.set_elems
}
pred set_singleton [ e: univ, s': Set ] {
	s'.set_elems = e
	one e
}
pred set_add [ s1: Set, e: univ, s': Set ] {
	s'.set_elems = s1.set_elems + e
	one e
}
fun set_cardinality [ s1: Set ]: one Int {
	#(s1.set_elems)
}
pred set_difference [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems - s2.set_elems
}
pred set_intersection [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems & s2.set_elems
}
pred set_union [ s1, s2, s': Set ] {
	s'.set_elems = s1.set_elems + s2.set_elems
}
pred set_in [ e: univ, s1: Set ] {
	e in s1.set_elems
	one e
	some s1.set_elems
}
pred set_subset [ s1, s2: Set ] {
	s1.set_elems in s2.set_elems
}
pred set_disjoint [ s1, s2: Set ] {
	disjoint[s1.set_elems, s2.set_elems]
}
pred set_equals [ s1, s2: Set ] {
	s1.set_elems = s2.set_elems
}
fact { all s1, s2: Set | s1.set_elems = s2.set_elems <=> s1 = s2 }
// ===== Seqs (resources/seq.als) =====
abstract sig Seq {
	// seq_rel: Int -> lone univ
	seq_rel: seq univ
} {
	isSeq[seq_rel]
}
pred seq_ranged [ from, to: Int, s': Seq ] {
	// { all i: Int | 0 <= i && i < sub[to, from] => s[i] = plus[from, i] }
	// #s = sub[to, from]
	s'.seq_rel = subseq[iden, from, sub[to, 1]]
}
pred seq_singleton [ e: univ, s': Seq ] {
	s'.seq_rel[0] = e
	#(s'.seq_rel) = 1
}
// NOTE: The sequence resulting from the wrapped 'append' operation may be
//		 truncated if the sequences are too long.
pred seq_append [ s1, s2, s': Seq ] {
	s'.seq_rel = append[s1.seq_rel, s2.seq_rel]
}
fun seq_length [ s: Seq ]: one Int {
	#(s.seq_rel)
}
fun seq_at [ s: Seq, i: Int ]: one univ {
	s.seq_rel[i]
}
pred seq_take [ s: Seq, i: Int, s': Seq ] {
	let to = sub[i, 1] |
	s'.seq_rel = subseq[ s.seq_rel, 0, to]
}
pred seq_drop [ s: Seq, i: Int, s': Seq ] {
	let to = sub[#s.seq_rel, 1] |
	s'.seq_rel = subseq[ s.seq_rel, i, to ]
}
pred seq_in [ s1: Seq, e: univ ] {
	e in elems[s1.seq_rel]
}
pred seq_update [ s: Seq, i: Int, e: univ, s': Seq ] {
	s'.seq_rel = setAt[s.seq_rel, i, e]
} 
// ===== Multiset (resources/multiset.als) =====
abstract sig Multiset {
	ms_elems: univ -> lone Int
} {
	all i: univ.ms_elems | gt[i, 0]
}
pred empty_multiset [ ms': Multiset ] {
    no ms'.ms_elems
}
pred multiset_singleton [ e: univ, ms': Multiset ] {
	ms'.ms_elems = (e -> 1)
}
pred multiset_add [ ms1: Multiset, elem: univ, ms': Multiset ] {
	ms'.ms_elems =	{ e: univ, v: Int | e in (elem - dom[ms1.ms_elems]) and v = 1 } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] - elem) and v = ms1.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] & elem) and v = add[ms1.ms_elems[e], 1] }
}
fun multiset_cardinality_fun [ ms: Multiset ]: one Int {
    sum ms.ms_elems[univ]
}
pred multiset_cardinality [ ms: Multiset, card: Int ] {
	card = (let s = { c: Int, e: univ | (e -> c) in ms.ms_elems } |
					(sum i: (s).univ | mul[#(s[i]), i]) )
	card >= 0
}
pred multiset_difference [ ms1, ms2, ms': Multiset ] {
	ms'.ms_elems = { e: univ, v: Int | e in (dom[ms1.ms_elems] - dom[ms2.ms_elems]) and v = ms1.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and
									e.(ms2.ms_elems) < e.(ms1.ms_elems) and
									v = minus[e.(ms1.ms_elems), e.(ms2.ms_elems)] }
}
pred multiset_intersection [ ms1, ms2, ms': Multiset ] {
	ms'.ms_elems = { e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and v = min[e.(ms1.ms_elems) + e.(ms2.ms_elems)] }
}
pred multiset_union [ ms1, ms2, ms': Multiset ] {
	ms'.ms_elems = { e: univ, v: Int | e in (dom[ms2.ms_elems] - dom[ms1.ms_elems]) and v = ms2.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] - dom[ms2.ms_elems]) and v = ms1.ms_elems[e] } +
				{ e: univ, v: Int | e in (dom[ms1.ms_elems] & dom[ms2.ms_elems]) and v = add[ms1.ms_elems[e], ms2.ms_elems[e]] }
}
pred multiset_subset [ ms1, ms2: Multiset ] {
	dom[ms1.ms_elems] in dom[ms2.ms_elems]
	{ all e: dom[ms1.ms_elems] | ms1.ms_elems[e] <= ms2.ms_elems[e] }
}
pred multiset_count [ ms1: Multiset, e: univ, c: Int ] {
	c = ms1.ms_elems[e]
} 
fun multiset_count_fun [ ms1: Multiset, e: univ ]: one Int {
	ms1.ms_elems[e]
} 
pred multiset_equals [ ms1, ms2: Multiset ] {
    ms1.ms_elems = ms2.ms_elems
}
sig Ref {
  next: lone Ref, 
  refTypedFields': set Ref
} {
  refTypedFields' = next
}

one sig NULL extends Ref {}
fact { NULL.refTypedFields' = none && no NULL.next }

one sig Store {
  nodes': one Set_Ref, 
  some': one Ref, 
  refTypedVars': set Ref
} {
  refTypedVars' = some'
}
one sig nodes_33_01 in Set_Ref {}
one sig some_34_01 in Ref {}
fact { Store.nodes' = nodes_33_01 }
fact { Store.some' = some_34_01 }

// Heap Chunks
// QA r@38@01 :: ((r@38@01 in nodes@33@01) ==> (inv@39@01(r@38@01) == r@38@01))
fact { (all r_38_01: Ref | (set_in[r_38_01, nodes_33_01] => (Fun.inv_39_01[r_38_01] = r_38_01))) }
// QA r :: ((inv@39@01(r) in nodes@33@01) ==> (inv@39@01(r) == r))
fact { (all r: Ref | (set_in[Fun.inv_39_01[r], nodes_33_01] => (Fun.inv_39_01[r] = r))) }
one sig t_36_01 in FVF_Ref {}
fact { all r: Ref | (set_in[Fun.inv_39_01[r], nodes_33_01] implies W else Z) = PermFun.next[r, t_36_01] }
fact { all r: Ref | (some fvf: (t_36_01) | one PermFun.next[r, fvf] and perm_less[Z, PermFun.next[r, fvf]]) <=> (one r.next) }
fact { all r: Ref, fvf: (t_36_01) | one PermFun.next[r, fvf] => (perm_at_most[PermFun.next[r, fvf], W]) }
one sig Preds {}

// Path Conditions
// QA r@41@01 :: ((r@41@01 in nodes@33@01) && ((r@41@01 in nodes@33@01) ==> !((Lookup(next, sm@43@01(), r@41@01) == Null))) ==> ((r@41@01 in nodes@33@01) ==> !((Lookup(next, sm@43@01(), r@41@01) == Null))) && (r@41@01 in nodes@33@01))
one sig sm_43_01 in FVF_Ref {}
fact { (all r_41_01: Ref | ((set_in[r_41_01, nodes_33_01] && (set_in[r_41_01, nodes_33_01] => !((Lookup.next[sm_43_01, r_41_01] = NULL)))) => ((set_in[r_41_01, nodes_33_01] => !((Lookup.next[sm_43_01, r_41_01] = NULL))) && set_in[r_41_01, nodes_33_01]))) }
// QA r :: ((inv@39@01(r) in nodes@33@01) ==> (inv@39@01(r) == r))
fact { (all r: Ref | (set_in[Fun.inv_39_01[r], nodes_33_01] => (Fun.inv_39_01[r] = r))) }
// QA r@38@01 :: ((r@38@01 in nodes@33@01) ==> !((r@38@01 == Null)))
fact { (all r_38_01: Ref | (set_in[r_38_01, nodes_33_01] => !((r_38_01 = NULL)))) }
// ($t@44@01 == Combine(_, _))
one sig t_44_01 in Snap {}
one sig temp_0' in Snap {}
fact { combine[Unit, Unit, temp_0'] && 
       (t_44_01 = temp_0') }
// (some@34@01 in nodes@33@01)
fact { set_in[some_34_01, nodes_33_01] }
// QA r :: ((inv@39@01(r) in nodes@33@01) ==> (Lookup(next, sm@43@01(), r) == Lookup(next, $t@36@01, r)))
fact { (all r: Ref | (set_in[Fun.inv_39_01[r], nodes_33_01] => (Lookup.next[sm_43_01, r] = Lookup.next[t_36_01, r]))) }
// QA r@38@01 :: ((r@38@01 in nodes@33@01) ==> (inv@39@01(r@38@01) == r@38@01))
fact { (all r_38_01: Ref | (set_in[r_38_01, nodes_33_01] => (Fun.inv_39_01[r_38_01] = r_38_01))) }
// !((Lookup(next, sm@43@01(), some@34@01) == Null))
fact { !((Lookup.next[sm_43_01, some_34_01] = NULL)) }
// ($t@35@01 == Combine(SortWrapper($t@36@01, Snap), $t@37@01))
one sig t_35_01 in Snap {}
one sig temp_1' in Snap {}
one sig temp_2' in Snap {}
one sig t_37_01 in Snap {}
fact { sortwrapper_new[t_36_01, temp_2'] && 
       combine[temp_2', t_37_01, temp_1'] && 
       (t_35_01 = temp_1') }
// QA r@41@01 :: ((r@41@01 in nodes@33@01) && ((r@41@01 in nodes@33@01) ==> !((Lookup(next, sm@43@01(), r@41@01) == Null))) ==> (Lookup(next, sm@43@01(), r@41@01) in nodes@33@01))
fact { (all r_41_01: Ref | ((set_in[r_41_01, nodes_33_01] && (set_in[r_41_01, nodes_33_01] => !((Lookup.next[sm_43_01, r_41_01] = NULL)))) => set_in[Lookup.next[sm_43_01, r_41_01], nodes_33_01])) }
// !((Lookup(next, sm@43@01(), some@34@01) == some@34@01))
fact { !((Lookup.next[sm_43_01, some_34_01] = some_34_01)) }
// ($t@37@01 == Combine(_, $t@40@01))
one sig temp_3' in Snap {}
one sig t_40_01 in Snap {}
fact { combine[Unit, t_40_01, temp_3'] && 
       (t_37_01 = temp_3') }
// ($t@40@01 == Combine(_, $t@44@01))
one sig temp_4' in Snap {}
fact { combine[Unit, t_44_01, temp_4'] && 
       (t_40_01 = temp_4') }

// Permission functions
one sig PermFun {
  next: (Ref -> FVF_Ref -> one Perm)
}

// Functions
one sig Fun {
  inv_39_01: (Ref -> lone Ref)
}

// Lookup functions
one sig Lookup {
  next: (FVF_Ref -> Ref -> lone Ref)
}
fact { all fvf: FVF_Ref, r: Ref | (one PermFun.next[r, fvf] and perm_less[Z, PermFun.next[r, fvf]]) => (Lookup.next[fvf, r] = r.next) }
// Other sorts
sig Set_Ref extends Set {} {
  set_elems in Ref
}
sig FVF_Ref {}

// No object unreachable from the Store
fact { Ref = Store.refTypedVars'.*refTypedFields' + NULL + (SortWrapper.wrapped <: Ref) + Store.nodes'.set_elems }

// Signarure Restrictions
fact { Set = Store.nodes' }
fact { FVF_Ref = t_36_01 + sm_43_01 }
fact { Snap = t_44_01 + temp_0' + t_35_01 + temp_1' + temp_2' + t_37_01 + temp_3' + t_40_01 + temp_4' + Unit }
fact { Perm = PermFun.next[Ref, FVF_Ref] + W + Z }
fact { Seq = none }
fact { Multiset = none }

run {} for 10 but 4 int, 1 Set, 2 FVF_Ref, 10 Snap, 3 Perm