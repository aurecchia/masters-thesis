## Working with the Viper IDE

### Prerequisites

- Linux:

      sudo apt install npm
      sudo npm install -g typescript

- `File > Open Folder` and open the `viper-ide/client` directory.
- Open another window via `File > New Window` and open the `viper-ide/server`
  directory in it.
- In both editors, launch the command palette (`Ctrl-Shift-P`) and execute `npm:
  Install Dependencies`
- Start the client debugger, via `F5` of via `Debug > Start Debugging`.
- Open a Viper file, in order to trigger the activation of the client extension.
  Once the client extension is active, start the server debugger via `F5` or via
  `Debug > Start Debugging`

