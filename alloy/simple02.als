/*
method foo(one: Ref, two: Ref)
	requires one != null
{
	var three: Ref := two;
	var four: Ref
}
*/

// This creates additional relations which lead to having isomorphic models
// fact { all r: HeapRef | some s: StoreRef | s.ref = r }

// References on the heap have no reason to exist unless we have something pointing to them,
// i.e. the set of all heap references is exactly the set of all heap references referred to
// by some store reference
fact { StoreRef.ref = HeapRef }

abstract sig StoreRef { ref: lone HeapRef }
abstract sig HeapRef {}

// All the store variables we know about
one sig var_one extends StoreRef {}
one sig var_two extends StoreRef {}
one sig var_three extends StoreRef {}
one sig var_four extends StoreRef {}

// requires one != null
fact { one var_one.ref }

// var three: Ref := two;
fact { var_three.ref = var_two.ref }


pred example {}
run example
