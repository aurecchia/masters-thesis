/*
field next: Ref
method foo(nodes: Set[Ref], head: Ref, first: Ref)
	requires forall n: Ref :: n in nodes ==> acc(n.next)
	requires forall n: Ref :: n in nodes ==> n.next != n
	requires forall n: Ref :: n in nodes ==> n.next in nodes
	requires head in nodes
    requires first in nodes
{
	var_other: Ref
}
*/

// References may be null by default
abstract sig StoreRef { ref: lone HeapRef }
sig HeapRef {}

// References on the heap have no reason to exist unless we have something
// pointing to them, i.e. the set of all heap references is exactly the set
// of all heap references referred to by some store reference
fact { StoreRef.ref = HeapRef }


sig QP_n_in_nodes extends HeapRef {
	next: lone HeapRef
}
fact { all n: QP_n_in_nodes | n.next != n }
fact { all n: QP_n_in_nodes | n.next in QP_n_in_nodes }


// All the store variables we know about
one sig var_head extends StoreRef {}
fact { one var_head.ref }  // head != null
fact { var_head.ref in QP_n_in_nodes }

one sig var_first extends StoreRef {}
fact { one var_first.ref }
fact { var_first.ref in QP_n_in_nodes }

one sig var_other extends StoreRef {}


pred example {}
run example
