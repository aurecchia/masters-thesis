/*
field next: Ref
method foo(nodes: Set[Ref], head: Ref)
	requires forall n: Ref :: n in nodes ==> acc(n.next)
	requires head in nodes
{
	assert true;
}
*/

// References may be null by default
abstract sig StoreRef { ref: lone HeapRef }
abstract sig HeapRef {}
abstract sig QuantifiedReference extends HeapRef {}

sig n_in_nodes extends QuantifiedReference {
	next: lone n_in_nodes
}

one sig n_in_nodes_QP {
	chunks: set QuantifiedReference
}
fact { n_in_nodes_QP.chunks = n_in_nodes }



// References on the heap have no reason to exist unless we have something
// pointing to them, i.e. the set of all heap references is exactly the set
// of all heap references referred to by some store reference
fact { StoreRef.ref = HeapRef }


// All the store variables we know about
one sig var_head extends StoreRef {}
fact { one var_head.ref }  // head != null

one sig var_first extends StoreRef {}
fact { one var_first.ref }  // first != null

one sig var_second extends StoreRef {}
fact { one var_second.ref }  // second != null

one sig var_current extends StoreRef {}
fact { var_current.ref = var_head.ref }

one sig var_first_prev extends StoreRef {}
fact { one var_first_prev.ref => var_first_prev.ref = var_current.ref }

// Trying out stuff
fact { var_first.ref != var_current.ref }
fact { var_first.ref != var_second.ref }
fact { var_first.ref != var_head.ref }
fact { var_second.ref != var_head.ref }
fact { var_first_prev.ref.next = var_first.ref }

// We constrain the set of references in each quantified structure to those
// that we definitely know are there
//fact { #n_in_nodes.chunks = 1 }
fact { n_in_nodes = var_head.ref + var_second.ref + var_first.ref + var_first_prev.ref }

pred example {}
run example
