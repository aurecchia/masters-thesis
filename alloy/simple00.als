/*
method example1(one: Ref)
{
	assert true;
}
*/

// References on the heap have no reason to exist unless we have something pointing to them,
// i.e. the set of all heap references is exactly the set of all heap references referred to
// by some store reference
fact { StoreRef.ref = HeapRef }

abstract sig StoreRef { ref: lone HeapRef }
abstract sig HeapRef {}

// All the store variables we know about
one sig var_one extends StoreRef {}

pred example1 {}
run example1
