/*
method foo(one: Ref, two: Ref)
	requires one != null
{
	var three: Ref := two;
}
*/

// References on the heap have no reason to exist unless we have something pointing to them,
// i.e. the set of all heap references is exactly the set of all heap references referred to
// by some store reference
fact { StoreRef.ref = HeapRef }

/* abstract sig StoreRef { ref: lone HeapRef } */
sig StoreRef {
  name: one String,
  ref: lone HeapRef
}
abstract sig HeapRef {}

// All the variables we know about
fact { one s: StoreRef | s.name = "one" }
fact { one s: StoreRef | s.name = "two" }
fact { one s: StoreRef | s.name = "three" }

// one != null
fact { all s: StoreRef | s.name = "one" => one s.ref }

// 
fact { all two: StoreRef, three: StoreRef | (two.name = "two" and three.name = "three") => (two.ref = three.ref) }

pred example {}
run example
