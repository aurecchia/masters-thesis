
abstract sig SymbVal {}
abstract sig Snap extends SymbVal {}
abstract sig IntSymbVal extends SymbVal {
  value: one Int
}
sig RefSymbVal extends SymbVal {
  next: lone RefSymbVal,
  val: lone IntSymbVal,
  successors': set RefSymbVal
} {
  successors' = next
}

abstract sig Combine extends Snap { l: one SymbVal, r: one SymbVal }
fact { all a, b: Combine | a = b => (a.l = b.l) && (a.r = b.r) }

one sig NULL extends RefSymbVal{} {
  successors' = none
}


one sig Store {
  y_8_01: one RefSymbVal,
  variables': set RefSymbVal
} {
  variables' = y_8_01
}

one sig Temp {
  t_10_01: one Snap,
  t_9_01: one Snap,
  t_11_01: one IntSymbVal,
  t_13_01: one IntSymbVal,
  t_12_01: lone RefSymbVal,
  t_14_01: lone RefSymbVal
}

one sig Combines {
  Combine_t_11_t_12: one Combine,
  Combine_t_13_t_14: one Combine,
}

// Combine collected from PCS
fact { Combines.Combine_t_11_t_12.l = Temp.t_11_01 &&
       Combines.Combine_t_11_t_12.r = Temp.t_12_01 }
fact { Combines.Combine_t_13_t_14.l = Temp.t_13_01 &&
       Combines.Combine_t_13_t_14.r = Temp.t_14_01 }

// From heap
fact { Store.y_8_01.next = Temp.t_14_01 }
fact { Store.y_8_01.val = Temp.t_13_01 }

// PCS
// ($t@10@01 == Combine($t@11@01, $t@12@01))
fact { Temp.t_10_01 = Combines.Combine_t_11_t_12 }
// !((y@8@01 == Null))
fact { !(Store.y_8_01 = NULL) }
// ($t@10@01 == Combine($t@13@01, $t@14@01))
fact { Temp.t_10_01 = Combines.Combine_t_13_t_14 }
// (!(($t@12@01 == Null)) ==> ($t@11@01 == 3))
fact { !((Temp.t_12_01 = NULL) => (Temp.t_11_01.value = 3)) }
// ($t@9@01 == $t@10@01)
fact { Temp.t_9_01 = Temp.t_10_01 }

// Negated last SMT query
// !($t@13@01 == 3)
/*fact { !(Temp.t_13_01.value = 3) }

pred generate() {}
run generate for 4*/

check { Temp.t_13_01.value = 3 } for 4
