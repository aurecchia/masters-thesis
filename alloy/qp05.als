/*
field val: Int
method foo(a: Array)
	requires i: Int :: 0 <= i && i < len(a) ==> acc(loc(a, i).val)
{
	var index: Int
	assume 0 <= index && index < len(a)
	var v: Int
    v := loc(a, index)
}
*/

// References may be null by default
abstract sig StoreRef { ref: lone HeapRef }
sig HeapRef {}

// References on the heap have no reason to exist unless we have something
// pointing to them, i.e. the set of all heap references is exactly the set
// of all heap references referred to by some store reference
fact { StoreRef.ref = HeapRef }


sig QP_array extends HeapRef {}


one sig var_v extends StoreRef {}
fact { one var_v.ref }  // v != null
fact { var_v.ref in QP_array }

one sig var_some extends StoreRef {}
fact { one var_some.ref }

pred example {}
run example
