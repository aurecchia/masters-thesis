/*
field next: Ref
method foo(some: Ref)
	requires acc(some.next)
{
	var_other: Ref
}
*/

// References may be null by default
abstract sig StoreRef { ref: lone HeapRef }
sig HeapRef {}

sig P_ref_next extends HeapRef {
	next: lone HeapRef
}

// References on the heap have no reason to exist unless we have something
// pointing to them, i.e. the set of all heap references is exactly the set
// of all heap references referred to by some store reference
fact { StoreRef.ref + P_ref_next.next = HeapRef }

// All the store variables we know about
one sig var_some extends StoreRef {}
fact { one var_some.ref }  // head != null
fact { var_some.ref in P_ref_next }


pred example {}
run example
