var executionTreeData = [
  {
    "kind": "Method",
    "value": "foo",
    "open": true,
    "prestate": {
      "store": [],
      "heap": [],
      "oldHeap": [],
      "pcs": []
    },
    "children": [
      {
        "kind": "WellformednessCheck",
        "open": true,
        "prestate": {
          "store": [
            { "value": "a -> a@0",
              "type": "Int"
            },
            {
              "value": "b -> b@1",
              "type": "Int"
            }
          ],
          "heap": [],
          "oldHeap": [],
          "pcs": []
        }
      },
      {
        "type": "execute",
        "pos": "6:5",
        "value": "b := 3 + a",
        "open": true,
        "prestate": {
          "store": [
            {
              "value": "a -> a@0",
              "type": "Int"
            },
            {
              "value": "b -> b@1",
              "type": "Int"
            }
          ],
          "heap": [],
          "oldHeap": [],
          "pcs": []
        },
        "children": [
          {
            "type": "evaluate",
            "pos": "6:19",
            "value": "3 + a",
            "open": true,
            "prestate": {
              "store": [
                {
                  "value": "a -> a@0",
                  "type": "Int"
                },
                {
                  "value": "b -> b@1",
                  "type": "Int"
                }
              ],
              "heap": [],
              "oldHeap": [],
              "pcs": []
            },
            "children": [
              {
                "type": "evaluate",
                "pos": "6:19",
                "value": "3",
                "open": true,
                "prestate": {
                  "store": [
                    {
                      "value": "a -> a@0",
                      "type": "Int"
                    },
                    {
                      "value": "b -> b@1",
                      "type": "Int"
                    }
                  ],
                  "heap": [],
                  "oldHeap": [],
                  "pcs": []
                }
              },
              {
                "type": "evaluate",
                "pos": "6:23",
                "value": "a",
                "open": true,
                "prestate": {
                  "store": [
                    {
                      "value": "a -> a@0",
                      "type": "Int"
                    },
                    {
                      "value": "b -> b@1",
                      "type": "Int"
                    }
                  ],
                  "heap": [],
                  "oldHeap": [],
                  "pcs": []
                }
              }
            ]
          }
        ]
      }
    ]
  }
]