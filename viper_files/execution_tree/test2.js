var executionTreeData = [
  {
    "kind": "Method",
    "value": "foo",
    "open": true,
    "prestate": {
      "store": [],
      "heap": [],
      "oldHeap": [],
      "pcs": []
    },
    "children": [
      {
        "type": "produce",
        "pos": "5:14",
        "value": "acc(r.val, write)",
        "open": true,
        "prestate": {
          "store": [
            {
              "value": "r -> r@0",
              "type": "Ref"
            }
          ],
          "heap": [],
          "oldHeap": [],
          "pcs": []
        },
        "children": [
          {
            "type": "evaluate",
            "pos": "5:18",
            "value": "r",
            "open": true,
            "prestate": {
              "store": [
                {
                  "value": "r -> r@0",
                  "type": "Ref"
                }
              ],
              "heap": [],
              "oldHeap": [],
              "pcs": []
            }
          },
          {
            "type": "evaluate",
            "pos": "5:14",
            "value": "write",
            "open": true,
            "prestate": {
              "store": [
                {
                  "value": "r -> r@0",
                  "type": "Ref"
                }
              ],
              "heap": [],
              "oldHeap": [],
              "pcs": []
            }
          }
        ]
      },
      {
        "kind": "WellformednessCheck",
        "open": true,
        "prestate": {
          "store": [
            {
              "value": "r -> r@0",
              "type": "Ref"
            }
          ],
          "heap": [],
          "oldHeap": [],
          "pcs": []
        }
      },
      {
        "type": "execute",
        "pos": "7:5",
        "value": "r.val := 2",
        "open": true,
        "prestate": {
          "store": [
            {
              "value": "r -> r@0",
              "type": "Ref"
            }
          ],
          "heap": [
            "r@0.val -> $t@1 # W"
          ],
          "oldHeap": [
            "r@0.val -> $t@1 # W"
          ],
          "pcs": []
        },
        "children": [
          {
            "type": "evaluate",
            "pos": "7:5",
            "value": "r",
            "open": true,
            "prestate": {
              "store": [
                {
                  "value": "r -> r@0",
                  "type": "Ref"
                }
              ],
              "heap": [
                "r@0.val -> $t@1 # W"
              ],
              "oldHeap": [
                "r@0.val -> $t@1 # W"
              ],
              "pcs": []
            }
          },
          {
            "type": "evaluate",
            "pos": "7:14",
            "value": "2",
            "open": true,
            "prestate": {
              "store": [
                {
                  "value": "r -> r@0",
                  "type": "Ref"
                }
              ],
              "heap": [
                "r@0.val -> $t@1 # W"
              ],
              "oldHeap": [
                "r@0.val -> $t@1 # W"
              ],
              "pcs": []
            }
          }
        ]
      }
    ]
  }
]