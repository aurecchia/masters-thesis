var executionTreeData = [
  {
    "kind": "Method",
    "value": "foo",
    "open": true,
    "prestate": {
      "store": [],
      "heap": [],
      "oldHeap": [],
      "pcs": []
    },
    "children": [
      {
        "kind": "WellformednessCheck",
        "open": true,
        "prestate": {
          "store": [
            {
              "value": "a -> a@0",
              "type": "Int"
            },
            {
              "value": "b -> b@1",
              "type": "Int"
            }
          ],
          "heap": [],
          "oldHeap": [],
          "pcs": []
        }
      },
      {
        "kind": "IfThenElse",
        "open": true,
        "children": [
          {
            "kind": "If",
            "type": "evaluate",
            "pos": "6:9",
            "value": "a > 10",
            "open": true,
            "prestate": {
              "store": [
                {
                  "value": "a -> a@0",
                  "type": "Int"
                },
                {
                  "value": "b -> b@1",
                  "type": "Int"
                }
              ],
              "heap": [],
              "oldHeap": [],
              "pcs": []
            },
            "children": [
              {
                "type": "execute",
                "pos": "7:9",
                "value": "b := 1",
                "open": true,
                "prestate": {
                  "store": [
                    {
                      "value": "a -> a@0",
                      "type": "Int"
                    },
                    {
                      "value": "b -> b@1",
                      "type": "Int"
                    }
                  ],
                  "heap": [],
                  "oldHeap": [],
                  "pcs": [
                    "a@0 > 10"
                  ]
                },
                "children": [
                  {
                    "type": "evaluate",
                    "pos": "7:14",
                    "value": "1",
                    "open": true,
                    "prestate": {
                      "store": [
                        {
                          "value": "a -> a@0",
                          "type": "Int"
                        },
                        {
                          "value": "b -> b@1",
                          "type": "Int"
                        }
                      ],
                      "heap": [],
                      "oldHeap": [],
                      "pcs": [
                        "a@0 > 10"
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "kind": "Else",
            "type": "evaluate",
            "pos": "<no position>",
            "value": "!(a > 10)",
            "open": true,
            "prestate": {
              "store": [
                {
                  "value": "a -> a@0",
                  "type": "Int"
                },
                {
                  "value": "b -> b@1",
                  "type": "Int"
                }
              ],
              "heap": [],
              "oldHeap": [],
              "pcs": []
            },
            "children": [
              {
                "type": "execute",
                "pos": "9:9",
                "value": "b := 2",
                "open": true,
                "prestate": {
                  "store": [
                    {
                      "value": "a -> a@0",
                      "type": "Int"
                    },
                    {
                      "value": "b -> b@1",
                      "type": "Int"
                    }
                  ],
                  "heap": [],
                  "oldHeap": [],
                  "pcs": [
                    "!a@0 > 10"
                  ]
                },
                "children": [
                  {
                    "type": "evaluate",
                    "pos": "9:14",
                    "value": "2",
                    "open": true,
                    "prestate": {
                      "store": [
                        {
                          "value": "a -> a@0",
                          "type": "Int"
                        },
                        {
                          "value": "b -> b@1",
                          "type": "Int"
                        }
                      ],
                      "heap": [],
                      "oldHeap": [],
                      "pcs": [
                        "!a@0 > 10"
                      ]
                    }
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
]