/*
    method foo(n:Int,b:Bool,c:Bool)
    {
      if ( b ) {
        assert true
      } else {
        assert true
        assert false
      }
    }
*/
[
    {
      "kind": "Method",
      "value": "foo",
      "open": true,
      "children": [
        {
          "kind": "WellformednessCheck",
          "open": true,
          "prestate": {
            "store": [
              { "value": "n -> n@3@01", "type": "Int" },
              { "value": "b -> b@4@01", "type": "Bool" },
              { "value": "c -> c@5@01", "type": "Bool" }
            ],
            "heap": [],
            "oldHeap": [],
            "pcs": []
          }
        },
        {
          "type": "evaluate",
          "pos": "3:8",
          "value": "b",
          "open": true,
          "prestate": {
            "store": [
              { "value": "n -> n@3@01", "type": "Int" },
              { "value": "b -> b@4@01", "type": "Bool" },
              { "value": "c -> c@5@01", "type": "Bool" }
            ],
            "heap": [],
            "oldHeap": [],
            "pcs": []
          }
        },
        {
          "type": "execute",
          "pos": "4:6",
          "value": "assert true",
          "open": true,
          "prestate": {
            "store": [
              { "value": "n -> n@3@01", "type": "Int" },
              { "value": "b -> b@4@01", "type": "Bool" },
              { "value": "c -> c@5@01", "type": "Bool" }
            ],
            "heap": [],
            "oldHeap": [],
            "pcs": [
              "b@4@01"
            ]
          }
        },
        {
          "type": "evaluate",
          "pos": "3:8",
          "value": "!b",
          "open": true,
          "prestate": {
            "store": [
              { "value": "n -> n@3@01", "type": "Int" },
              { "value": "b -> b@4@01", "type": "Bool" },
              { "value": "c -> c@5@01", "type": "Bool" }
            ],
            "heap": [],
            "oldHeap": [],
            "pcs": []
          },
          "children": [
            {
              "type": "evaluate",
              "pos": "3:8",
              "value": "b",
              "open": true,
              "prestate": {
                "store": [
                  { "value": "n -> n@3@01", "type": "Int" },
                  { "value": "b -> b@4@01", "type": "Bool" },
                  { "value": "c -> c@5@01", "type": "Bool" }
                ],
                "heap": [],
                "oldHeap": [],
                "pcs": []
              }
            }
          ]
        },
        {
          "type": "execute",
          "pos": "6:5",
          "value": "assert true",
          "open": true,
          "prestate": {
            "store": [
              { "value": "n -> n@3@01", "type": "Int" },
              { "value": "b -> b@4@01", "type": "Bool" },
              { "value": "c -> c@5@01", "type": "Bool" }
            ],
            "heap": [],
            "oldHeap": [],
            "pcs": [
              "!b@4@01"
            ]
          }
        },
        {
          "type": "execute",
          "pos": "7:5",
          "value": "assert false",
          "open": true,
          "prestate": {
            "store": [
              { "value": "n -> n@3@01", "type": "Int" },
              { "value": "b -> b@4@01", "type": "Bool" },
              { "value": "c -> c@5@01", "type": "Bool" }
            ],
            "heap": [],
            "oldHeap": [],
            "pcs": [
              "!b@4@01"
            ]
          }
        }
      ]
    }
  ]