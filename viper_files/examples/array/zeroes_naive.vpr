import "array.vpr"


// Permissions are too broad and after the recursive call the 0 at offset might
// have been changed, thus the postcondition fails.
method zeros(a:Array, offset:Int, n:Int)
  requires forall i : Int :: 0 <= i && i < len(a) ==> acc(loc(a,i).val)
  requires 0 <= offset && 0 < n && offset + n < len(a)

  ensures forall i : Int :: 0 <= i && i < len(a) ==> acc(loc(a,i).val)
  ensures forall i: Int :: offset <= i && i < offset + n ==> loc(a,i).val == 0
{
  loc(a,offset).val := 0
  if(n > 1) {
    zeros(a, offset + 1, n-1)
  }
}

