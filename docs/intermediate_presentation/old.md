class: left, overbar, no_number, title_slide

# Creating an Advanced Debugger for<br> Quantified Permissions

## Master's Thesis Initial Presentation<br>26 April 2018

<div class="names-block">
  <div>Alessio Aurecchia</div>
  <div></div>
  <em>Supervised by</em>
  <div>Arshavir Ter-Gabrielyan</div>
</div>

???

- QP: Integral and powerful feature
- Working with them can become difficult

---

class: center-content

# Why / Motivation

```
define LinkedList(nodes)
    !(null in nodes) 
    && forall n: Ref :: n in nodes ==> acc(n.next)
    && forall n: Ref :: n in nodes ==> acc(n.val)
    && forall n: Ref ::
        n in nodes && n.next != null ==> n.next in nodes

field next: Ref
field val: Int
```

???
Say we have a LinkedList definition

---

name: swap
class: center-content

# Why / Motivation

```
method swap(nodes: Set[Ref], x: Ref, y: Ref)
  requires Linkedlist(nodes)
  requires x in nodes && y in nodes
{
  ...
}
```

???
- We are verifying swap
- To understand what's going on we often draw diagrams

---

template: swap

<div class="expand"><img width="60%" src="img/swap_0.svg" /></div>

---

template: swap

<div class="expand"><img width="60%" src="img/swap_1.svg" /></div>

---

template: swap

<div class="expand"><img width="60%" src="img/swap_2.svg" /></div>

---

template: swap

<div class="expand"><img width="60%" src="img/swap_3.svg" /></div>

---

template: swap

<div class="expand"><img width="60%" src="img/swap_4.svg" /></div>

---

template: swap

<div class="expand"><img width="60%" src="img/swap_5.svg" /></div>

---

# Why / Motivation

- Correct? (satisfies specification?)

???
- No guarantees reflected what was actually happening
- No guarantees that it actually followed the spec


---

# Why / Motivation

- Correct? (satisfies specification?)
- Corner cases?

<div class="expand"><img width="90%" src="img/corner_case.svg" /></div>

???
- We may miss corner cases!

---

class: center-content

# We want...

... to automate the generation of heap diagrams for QPs.

... the user to be able to compare the heap in different states.

... the user to be able to interact with the visualization.

---

class: small-code, center-content

# Specification vs. Mental Model

```
domain Array {
  function loc(a: Array, i: Int): Ref
  ...
}
```

???

- Raises a series of research questions / challenges
- Most fundamental one?

--

<div class="expand"><img width="70%" src="img/array.svg" /></div>

???

We think of the elements as ordered in memory, distinguished by both arguments.

---

class: small-code, center-content

```
domain Chessboard {
  function black(c: Chessboard, i: Int): Ref
  function white(c: Chessboard, i: Int): Ref
  ...
}
```
???

A user may define a chessboard, where `black` and `white` access the i-th elements.

--

<div class="expand"><img width="90%" src="img/chessboard_wrong.svg" /></div>

???
If we follow the previous assumption we end up with a visualization that does not make sense.

---

class: small-code, center-content

```
domain Chessboard {
  function black(c: Chessboard, i: Int): Ref
  function white(c: Chessboard, i: Int): Ref
  ...
}
```

<div class="expand"><img width="90%" src="img/chessboard_cross.svg" /></div>

---

class: small-code, center-content

```
domain Chessboard {
  function black(c: Chessboard, i: Int): Ref
  function white(c: Chessboard, i: Int): Ref
  ...
}
```

<div class="expand"><img width="90%" src="img/chessboard.svg" /></div>

???
What the user has in mind is more difficult.

This calls for a visualization "type" that is based on few assumptions and a "best-effort" visualization that tries to align with the user's mental model.

The fall-back visualization has to always be possible and correct, though possibly not-so-useful, being a superset.

---

# Abstract vs. Concrete

```
method swap(nodes: Set[Ref], x: Ref, y: Ref)
  requires Linkedlist(nodes)
  requires x in nodes && y in nodes
  ...
```
???
- Consider the `swap` example again
- We are working with _symbolic states_, so the visualization might consider all possible situations.

--

<div class="expand"><img width="45%" src="img/linked_list_abstract.svg" /></div>
???
- Not always useful, what do we actually know?
- There are other tools, for bounded model checking, that allow to visualize concrete situations from generic specs, such as Alloy

---

<div class="expand"><img width="100%" src="img/alloy.png" /></div>

---

# Abstract vs. Concrete

```
method swap(nodes: Set[Ref], x: Ref, y: Ref)
  requires Linkedlist(nodes)
  requires x in nodes && y in nodes
  ...
```

<div class="expand"><img width="80%" src="img/concrete_examples.svg" /></div>

???
It would be interesting to consider a similar approach, where we show the user concrete situattions.

Again, the question of how to generate interesting instances, choosing a "meaningful" one are to be investigated.

---

class: gray-out-code

```
*method zeros(a: Array, offset: Int, n: Int)
  requires forall i: Int ::
      0 <= i && i < len(a) ==> acc(loc(a,i).val)
  requires 0 <= offset && 0 < n && offset + n < len(a)

  ensures forall i: Int ::
      0 <= i && i < len(a) ==> acc(loc(a,i).val)
  ensures forall i: Int ::
      offset <= i && i < offset + n ==> loc(a,i).val == 0
{
  loc(a,offset).val := 0
  if(n > 1) {
    zeros(a, offset + 1, n - 1)
  }
}
```

???

Third challenge is that of visualizing the permissions themselves

---

class: gray-out-code

```
method zeros(a: Array, offset: Int, n: Int)
* requires forall i: Int ::
*     0 <= i && i < len(a) ==> acc(loc(a,i).val)
* requires 0 <= offset && 0 < n && offset + n < len(a)

  ensures forall i: Int ::
      0 <= i && i < len(a) ==> acc(loc(a,i).val)
  ensures forall i: Int ::
      offset <= i && i < offset + n ==> loc(a,i).val == 0
{
  loc(a,offset).val := 0
  if(n > 1) {
    zeros(a, offset + 1, n - 1)
  }
}
```
---
class: gray-out-code

```
method zeros(a: Array, offset: Int, n: Int)
  requires forall i: Int ::
      0 <= i && i < len(a) ==> acc(loc(a,i).val)
  requires 0 <= offset && 0 < n && offset + n < len(a)

  ensures forall i: Int ::
      0 <= i && i < len(a) ==> acc(loc(a,i).val)
  ensures forall i: Int ::
      offset <= i && i < offset + n ==> loc(a,i).val == 0
{
* loc(a,offset).val := 0
  if(n > 1) {
    zeros(a, offset + 1, n - 1)
  }
}
```
---
class: gray-out-code

```
method zeros(a: Array, offset: Int, n: Int)
  requires forall i: Int ::
      0 <= i && i < len(a) ==> acc(loc(a,i).val)
  requires 0 <= offset && 0 < n && offset + n < len(a)

  ensures forall i: Int ::
      0 <= i && i < len(a) ==> acc(loc(a,i).val)
  ensures forall i: Int ::
      offset <= i && i < offset + n ==> loc(a,i).val == 0
{
  loc(a,offset).val := 0
* if(n > 1) {
*   zeros(a, offset + 1, n - 1)
* }
}
```
---
class: gray-out-code

```
method zeros(a: Array, offset: Int, n: Int)
  requires forall i: Int ::
      0 <= i && i < len(a) ==> acc(loc(a,i).val)
  requires 0 <= offset && 0 < n && offset + n < len(a)

* ensures forall i: Int ::
*     0 <= i && i < len(a) ==> acc(loc(a,i).val)
* ensures forall i: Int ::
*     offset <= i && i < offset + n ==> loc(a,i).val == 0
{
  loc(a,offset).val := 0
  if(n > 1) {
    zeros(a, offset + 1, n - 1)
  }
}
```

---

class: small-code, center-content

<div class="expand"><img width="100%" src="img/zeroes_before.svg" /></div>

???
We could visualize the situation like this

--

```
zeros(a, offset + 1, n - 1)
```

--

<div class="expand"><img width="100%" src="img/zeroes_after.svg" /></div>

???
It would be useful if the debugger could visualize the fact that we don't know if the values are preserved.

---

class: small-code, center-content

<div class="expand"><img width="100%" src="img/zeroes_before_half.svg" /></div>

???
One of the possible fixes could be visualized as follows.

```
// Recurse
zeros(a, offset + 1, n - 1)
```

<div class="expand"><img width="100%" src="img/zeroes_after_half.svg" /></div>

???
The question is whether we have enough information and whether we are capable of building such a diagram.

---

<table style="width: 100%">
  <colgroup>
    <col style="width: 40%">
    <col style="width: 60%">
  </colgroup>
  <tr>
    <td><strong>Gathering Information</strong></td>
    <td>SymbExLogger</td>
  </tr>
--
  <tr>
    <td><strong>Current Infrastructure</strong></td>
    <td>VS Code client + VS Code Server + Viper Server</td>
  </tr>
--
  <tr>
    <td><strong>Implementing New Features</strong></td>
    <td>In the current infrastructure</td>
  </tr>
--
  <tr>
    <td><strong>External Tools?</strong></td>
    <td>
      <ul>
        <li>Bounded model-checking</li>
        <li>Limited Preview Pane</li>
      </ul>
    </td>
  </tr>
</table>

---

<div class="expand"><img width="98%" src="img/ide.png" /></div>

---

<table style="width: 100%">
  <colgroup>
    <col style="width: 40%">
    <col style="width: 60%">
  </colgroup>
  <tr>
    <td><strong>Gathering Information</strong></td>
    <td>SymbExLogger</td>
  </tr>
  <tr>
    <td><strong>Current Infrastructure</strong></td>
    <td>VS Code client + VS Code Server + Viper Server</td>
  </tr>
  <tr>
    <td><strong>Implementing New Features</strong></td>
    <td>In the current infrastructure</td>
  </tr>
  <tr>
    <td><strong>External Tools?</strong></td>
    <td>
      <ul>
        <li>Bounded model-checking</li>
        <li>Limited Preview Pane</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td><strong>Existing Debugger Features</strong></td>
    <td>
      <ul>
        <li>Static visualization (store + heap)</li>
        <li>State Navigation</li>
        <li>Simple and advanced modes</li>
      </ul>
    </td>
  </tr>
</table>

---

class: center-content

# Schedule


<table id="schedule">
  <tr><td class="schedule-core">C</td>
      <td>New Infrastructure Design</td></tr>
  <tr><td class="schedule-core">C</td>
      <td>Design QP Visualization</td></tr>
  <tr><td class="schedule-core">C</td>
      <td>Basic QP Support (Fall-back)</td></tr>
  <tr><td class="schedule-ext">E</td>
      <td>"Best" QP Support (assuming all information available)</td></tr>
  <tr><td class="schedule-ext">E</td>
      <td>Heuristics to gather information</td></tr>
  <tr><td class="schedule-ext">E</td>
      <td>Features from existing debugger</td></tr>
  <tr><td class="schedule-ext">E</td>
      <td>Testing, CI, Documentation, Additional features</td></tr>
</table>

