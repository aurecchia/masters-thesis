class: left, overbar, no_number, title_slide

# Creating an Advanced Debugger for<br> Symbolic Execution

## Master's Thesis Intermediate Presentation<br>5 July 2018

<div class="names-block">
  <div>Alessio Aurecchia</div>
  <div></div>
  <em>Supervised by</em>
  <div>Arshavir Ter-Gabrielyan</div>
</div>

???

- QP: Integral and powerful feature
- Working with them can become difficult

---

# Main Use Case: Counterexample Visualization

We have a verification failure and we want to show the user a diagram of one
particular instance of the program that leads to this failure.

TODO: Add ViperIDE screenshot with counterexample mockup

---

# When do we come into play?

When verifying the program we are checking if the following formula can be
satisfied.

$$
\lnot ( PC \vDash PO )
$$

We have 4 possible outcomes from z3:

- **UNSAT**: Program is verified
- **SAT**: There is a counterexample
- **unknown**: No idea
- **timeout**: We got stuck somewhere

The last three cases are of our interest: we want to generate a counterexample
in these situations.

**TODO:** In case we get SAT, what exactly do we get back from z3? Can we use it
in any way? What do we know in the other cases? What if the code is actually
fine?

---


# How Can We Generate Counterexamples

We encode the information we have from the symbolic execution trace and use
Alloy to come up with valid instances of our program.

              SymbExLog                model.als
    ViperIDE -----------> trace2Alloy -----------> Alloy
       ^                                             |
       +---------------------------------------------+
                      Counterexample(s)

---


# What Are the Problems With This Approach

    1. **Bounded model-checking** ==> under-approximation.
                                      We can live with this

    2. **Theory incompleteness** ==> over-approximation.
                                     Spurious counterexamples.

???

1. **Bounded model-checking**: Alloy performs a bounded exploration of the
   search space. This means that we may get an under-approximation.

2. **Theory incompleteness**: Alloy does not deal well with theory-related
   problems. If we don't encode them in the model we get an over-apporximation
   of the counterexample set as a result.

---


# How Can We Deal With These Two Problems

1. **Bounded model-checking**: We can live with it. Generating overwelmingly
   large counterexamples would not be usefult anyways. Moreover, we can allow
   the user to adjust the bounds for model-checking, enabling them to
   potentially explore a larger set of instances.
2. **Theory incompleteness**: Is more of a challenge, we can skip encoding facts
   for problems Alloy is not good at solving, but we may get a large set of
     spurious counterexamples.

---


# Idea

We could use Alloy for what it is good at and additionally employ the SMT solver
(z3) for solving the problems that Alloy struggles with.

 ------------------------------------------------
       Alloy                Z3
 ----- -------------------  ---------------------
 Pros  Complete search(*)   Support for theories
                            Sound

 Cons  Theories             Incomplete search
 ------------------------------------------------

<i>(*): In a certain bound.</i>

**TODO:** Need more details on this, to understand exactly what Alloy can and
cannot do.

---


# How Can We Implement That

We have the underlying assumption that we can split the set of path conditions
into two distinct sets, that we can employ in different moments in the pipeline.

$$
\lnot (PC \vDash PO) \equiv \lnot (PC_1 \land PC_2 \vDash PO)
$$

Here, $PC_1$ corresponds to the set of facts that we can easily encodt into
Alloy and that it can easily work with (QPs and permissions, null-ness,
aliasing). The rest, $PC_2$, are facts that Alloy would not help us with or for
which we don't have a good encoding.

---


# Separating Facts In The Pipeline

$$
\lnot (PC_1 \land PC_2 \vDash PO)
$$

We only use $PC_1$ as additional facts when encoding the Alloy model. What we
get back from Alloy is a set of counterexample candidates: this is an
overapproximation of the actual counterexamples.

We then re-encode these candidates into a format that z3 can work with and add
the rest of the facts ($PC_2$). This second step allow us to filter-out spurious
counterexamples from the set of candidates (this is the _post-filtering phase_).

      SymbExLog                PC1.als          CE candidate(s)
     -----------> trace2Alloy ---------> Alloy -----------------+
                                                                |
    +-----------------------------------------------------------+
    |
    | CE Candidate(s)               candidate.smt2       Counterexample(s)
    +-----------------> graph2smt2 ----------------> Z3 ------------------->
                                                     ^
                                    PC2.smt2 --------+

---


# Why "Post"-filtering?

The post-filtering phase is called like that because it happens after we
generated candidates via Alloy. It is also possible to perform some filtering
before encoding the program facts into an Alloy model. This would be the
_pre-filtering phase_.

                                Processed
          "Raw" SymbExLog       SymbExLog                PC1.als
         -----------------> Z3 -----------> trace2Alloy --------->
                            ^
    constrains.smt2 --------+

Here, it is possible to constrain the set of program instances so we model only
the ones we are interested in. We can allow the user to provide his own
constraining facts.

**TODO:** Define exactly what type of filtering happens here.

---


# The Whole Pipeline

    +----------------------------------------+
    |                                        |
    |   NICE GRAPH WITH THE WHOLE PIPELINE   |
    |                                        |
    +----------------------------------------+

Highlight the 4 phases of counterexample generation:

  - Pre-filtering
  - Candidate generation
  - Post-filtering
  - Mapping to a diagram

Highlight what is the focus of this project.

---

class: middle, dark, no_number, center-title, uppercase_titles, title_slide

# Prototype

---


# What Are Some Technical Challenges?

- SymbExLog translation of quantified permissions
- Smart caching of generated instances
- Mapping of Alloy instances to Graphviz
- Interface with user-provided constraints

