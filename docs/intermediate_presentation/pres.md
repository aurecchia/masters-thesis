# TODO: 
- Add retrieval of failed assertion as a technical challenge?

# Counterexample Visualization

- The idea of a debugger is to help the user fix problems in the code
- In our specific case, we want to visualise counterexamples to the program's
  specification, so that the user can more easily identify problematic
  situations.

# When does this apply

- In general, when we verify programs, we are trying to find a situation where
  the path conditions do not lead to the proof objective being verified.

- Of course, we have the two trivial results that we can expect from the SMT
  solver.
- If we get back UNSAT, we know everything is fine.
- If we get SAT, then there is a verification error. A model represents a
  (partial) counterexample to the verification proof.

- Obviously, those are the best-case solutions. In practice, we might get
  unknown and timeouts from the solver.
- We might get unknown because of incompleteness in the SMT solver, because it
  ran out of resources, or other such problems. Still, in this case we might
  still get back a partial counterexample from the solver.
- We may get timeouts as well, if for example the SMT solver gets stuck
  expanding quantifiers.

- For the purpose of counterexample generation, only the last 3 cases are
  relevant (those in which we do not prove the program correct).
- In these situations, we want to encode the facts we know about the program and
  try to generate concrete instances of it, that lead to the verification
  failure we just experienced.

# Generating counterexamples

- How do we get information about the execution and how do we generate the
  conuterexample instances.
- When we send a query to Silicon, we get back the result and the SymbExLog, a
  "summary" of the execution.
- In the SymbExLog we have also a recording of the trace that failed, complete
  with path conditions and information abut the heap and the store.

- An idea of what we can do is encode the path conditions and memory information
  into an Alloy model.
- Running this model through Alloy we get a set of counterexamples back.

# Problems with this Approach

- There are two main problems with this approach: boundedness and theory
  incompleteness.
- The first one stems from the way Alloy works. Since it explores the search
  space in a bounded fashion, what we get back is actually an
  under-approximation of the possible counterexamples
- The second problem arises because of some weaknesses Alloy has in dealing with
  theories and possibly also because of our encoding of the model.
- The first problem is not necessarily a problem since we don't want to
  counterexamples that are too compicated. Moreover, we also have control over
  the bounds in which Alloy operates, so we can adjust them at our leisure
  (dinamycally or even allowing the user to set them).
- The second problem is less desirable, expecially since over-approximating
  leads to getting back spurious counterexamples. How can we deal with this?

# The strong points

- Let's consider the strong points of the tools we have at our disposal.
- Alloy performs a complete search (in a given bound) but has difficulties with
  theories.
- On the other hand, the SMT solver has got good support for theories and
  outputs sound results, despite performing an incomplete search.
- What if we combine the two to solve the over-aproximation problem?

# Splitting the problem

- The assumption behind using a combination of the two tools is that we can
  split the problem we are working with into two sets of facts: one that's
  relevant for Alloy and one that's relevant for Z3.
- In practice, we can encode into the Alloy model all facts about null-ness,
  aliasing, references / QPs and permissions, and check "all the rest" with Z3.

- Our pipeline for counterexample generation has to change a bit.
- We still get our SymbExLog from Silicon, but as the first step, we split its
  facts into the two sets we just talked about.
- The P_{Alloy} facts will still get encded into an Alloy model, that will
  generate us a set of instance. The difference is that these instances have to
  be considered counterexample candidates, not proper counterexamples. This is
  an approximation of the counterexample set we are looking for (modulo bound).
- After we get our candidate(s), we encode them into SMT and here we join them
  with the facts that we had left behind from the split. This specification gets
  sent to Z3 and what we get back is our final set of counterexamples, which
  will be a more precise over-approximation of our counterexamples (still modulo
  bounds)

# The pipeline

- The final pipeline of our system looks like this. There are substantially two
  phases to it: the candidate generation phase which involves Alloy, and the
  filtering phase which involves Z3 filtering out the spurious conuterexamples.
- In addition to this, we can add an additional phase in the beginning, a
  partitioning phase. This partitioning phase can be used to integrate
  additional constraints into the counterexample search. These constraints may
  come from the user or could be gathered in other way.
- These additional constraints can be generated by substantially duplicating the
  pipeline and adding the constraints (and their negation) as an assumptions to
  the two parts.
- In one pipeline we generate counterexamples assuming some constraing C holds
  (this might be, for example, some_var != null). In the other pipeline we
  assume this same constrains does *not* hold. What this would allow us to do is
  allow the user to add his own assumptions about the issue being solved and to
  make deductions based on the results of the two pipelines. For example, if
  both of the pipelines still fail with the same original problem, then it means
  that the additional assumption is irrelevant to the problem being debugged. If
  only one of the two pipelines fails, then it means that the additional
  constraint is a key fact in the failure being debugged. We have to be careful
  in adding these constraints, because they could lead us to be unsound.
- It is important to note that partitioning on these constraints is not an
  approximation, because we are considering both the case in which they hold and
  the one in which they do not. So we are effectively solving the same problem.

# The focus of this thesis

- This thesis will focus only on part of this counterexample-generation system.
- I am going to try and focus on the extraction the program's facts from the
  SymbExLog and their translation into a valid Alloy model, on the filtering of
  some spurious instances (without the use of Z3) and of the mapping of the
  relvant instances into a diagram. In addition to that I will add a way for the
  user provide additional constraints for the counterexample search, though these
  will not be used in partitioning, but as actual filters (thus allowing to obtain
  an underapproximation of the counterexamples set).

# Prototype

# Technical Challenges

- There are of course some challenging points in the implementation of this
  work.
- First is the splitting of potentially complex formula into those relevant for
  Alloy, and those that cannot be mapped into a model. There is some existing
  work on splitting formulas for verification, such as that implemented in the
  Jahob verification tool. Though it does not exactly align with our needs, it
  certainly provides some useful approaches to the problem. Another option is
  simple "cherry-picking" of relevant facts.
- Another challenge (probably the most important) is the translation of the
  facts from the SymbExLog into the Alloy model. To tackle this we will almost
  certainly have to enhance the trace with more structured information on the path
  conditions.
- The interface with user-provided constraints is also a challenge, as they may
  potentially contain arbitrarily complex Viper expressions.
- An additional challenge (yet, not necessarily a very important one) is the
  caching of the generated instances, for performance issues.


