# Problem Statement (3 - 5 mins)

- We want to show small, concrete, visual counterexamples to the program's
  Specification.
- Essentially a bounded model checking problem: PC \land \lnot PO -> "Small
  Counterexample Hypothesis".
- Why Viper to Alloy (and not use z3?)
- Alloy language
  - Structural models via relational constraints
  - Transitive closure allows powerful definitions
  - Close to Viper (?)
  - Powerful abstractions.
- Alloy analyser
  - Complete search in bounds

# QP Example (8 mins)


# Limitations?
