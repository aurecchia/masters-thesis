\chapter{Architecture}\label{sec:architecture}

In this section we discuss the architecture of the Viper IDE extension
at the time the project started, and how we changed it in order to both support
the features of the new debugger and to allow extending it in the future.

\section{The Existing Infrastructure}\label{the-existing-infrastructure}

The Viper IDE is implemented as an extension for the Visual Studio Code editor.
Architecturally, it comprises three main components: the language client, the
language server, and the debugger adapter. Additionally, the language server
starts and communicates with \emph{ViperServer}, an external component that
manages the instantiation and execution of the verification backends.
ViperServer and the verification backends are downloaded and setup by the
extension automatically, but are not part of it.

The fist components, the language client and language server, are decoupled from
each other and communicate via the \emph{Viper Protocol} and the \emph{Language
Server
Protocol}\footnote{\url{https://code.visualstudio.com/docs/extensions/example-language-server\#_a-brief-overview-of-language-server-and-language-server-protocol}}.
They implement the two parts needed by Visual Studio Code for a language server
to work. On the other hand, the code that constitutes the debugger exists both
in the client and the server components and is tightly coupled with them.

There are two problems with the modularity this design:

\begin{itemize}
\tightlist
\item
  Components that have orthogonal responsibilities are coupled together
  and depend on each other to function.
\item
  It is not possible to \emph{only} install the components providing
  integration with the verification backends (language client and
  server), but one must also install the debugger, though it might not
  be needed.
\end{itemize}

If we envision integrating the verification environment with new tools in the
future, then this type of architecture is not ideal. Moreover, both the old
debugger and the new one only target the Silicon backend, therefore the design
of the infrastructure should accommodate the possibility of having multiple
debugger extensions installed at the same time, one for each of the verification
backends. Moreover, the architecture should allow not making the debugger an
explicit dependency of tools that have nothing to do with symbolic execution
(e.g.\ the Carbon backend).

Another thing to note is that the infrastructure of the main extension has been
partially reworked after the completion of Kälin's thesis, but the code for the
debugger has not been kept up to date, so the original debugging features are
not active in the current version of the extension.

In addition to that, the debugger also directly depends on external tools, in
order to retrieve the symbolic execution trace. When silicon is run with the
\lstinline{--ideModeAdvanced} flag, it writes the symbolic execution trace to
a file in a temporary location. The debugger then reads the trace from this
file. Retrieving the symbolic execution trace this way is less than ideal,
because of I/O performance reasons. This problem can be solved easily with the
new architecture, where the backends and the IDE communicate via message passing
through ViperServer.


\section{The New Infrastructure}\label{the-new-infrastructure}

The most important architectural change, with respect to the implementation of
the previous debugger, is that the new one ``lives'' in a completely separate
extension. This design choice was made to avoid forcing users to install the
debugger extension if they have no need for its features, especially since the
debugger only targets the Silicon backend and because it is currently in an
experimental state. We will refer to the existing extension (which integrates VS
Code with the verification backends) as the \emph{main extension}, and to the
new one as the \emph{debugger extension}.

The debugger depends on the main extension to work correctly, therefore lists it
as an explicit dependency in its extension manifest file. Visual Studio Code
makes sure that this dependency is satisfied by preventing the debugger to be
installed if the main extension is not present.

Since the debugger is separate from the main extension (but still
requires information about the verification process in order to
function), we had to update the main extension to support
communication with other extensions. In addition to that, the debugger
also needs to interact with a panel in the user interface, which is also
a completely separate component. In the following sections, we will
discuss how these separate parts of the system communicate with
each other.

\begin{figure}[htb]
  \centering
  \includegraphics{img/architecture.pdf}
  \caption[The architecture of the system]{ The architecture of the system. The
    \textit{Viper IDE} and \textit{Viper Debugger} boxes denote, respectively,
    the main Viper extension and the new debugger extension. They both interact,
    independently, with Visual Studio Code. Viper Server is responsible for
    running the verification backends.}
  \label{fig:architecture}
\end{figure}

A diagram of the infrastructure outlining the main components and the
communication channels between them can be seen in
\Cref{fig:architecture}.

\subsection{Communication with the Main Extension}\label{communication-with-the-main-extension}

Visual Studio Code extensions can return an object from their
\lstinline{activate}\footnote{https://code.visualstudio.com/docs/extensions/example-hello-world\#\_generated-code}
method (the main entry point of each extension). This object can be
retrieved and used by other extensions at runtime. The main Viper
extension has been updated to export a
\lstinline{ViperApi} object that allows other extensions
(in this case the debugger) to interact with the verification process.
Though currently this API only provides functionalities specifically
needed by the debugger, this approach allows it to be easily extended to
support other extensions that can interact with Viper, without having to
drastically modify the implementation of the main extension.

The Viper API provides some utility methods that allow the debugger to access
the last file that was verified or to retrieve the state of the backend. In
addition to that, it also allows other extensions to register a notification
handler that is called when a verification terminates or when ViperServer sends
messages that the main extension does not know how to handle.

When the Silicon backend is configured with the \lstinline{--ideModeAdvanced}
flag (more details in appendix \Cref{app:configuration}), it will produce
additional messages for the debugger, that are delivered to the main extension
via ViperServer. The main extension does not know how to handle these additional
messages, therefore it will notify all registered handlers for them (if there
are any). When the debugger is first started, it retrieves the
\lstinline{ViperApi} object and registers a handler for verification completion
events and for additional server messages. Thus, it can receive messages
containing debugging information from ViperServer via the main extension, and be
notified with verification completion events.

\subsection{ViperServer and Silicon}\label{viperserver-and-silicon}

The old debugger, as mentioned before, retrieved the symbolic execution trace
from a file. ViperServer (along with some other parts of the reporting
infrastructure) has been updated to serialize the Symbolic Execution Log to
JSON, via the \emph{Spray JSON}\footnotemark library (which was already used to
for the rest of the communication with the backends). The trace is now streamed
via HTTP by ViperServer, and finally gets to the Debugger after passing through
the main extension and the ViperApi.

\footnotetext{\url{https://github.com/spray/spray-json}}

The Symbolic execution log has also been extended to contain more
information, needed by the debugger to operate. These additional pieces
of information are the postcondition axioms declared by Silicon for the
functions in the program being verified, the
\lstinline{pTaken} macros generated when permissions are
removed from heap chunks, and the last query that was performed to
the SMT solver before the verification failed (in case the SMT solver was not
able to disprove an assertion).

\subsection{The Webview Panel}\label{the-webview-panel}

The existing debugger provided a side-panel to display the debugger information.
The panel was implemented using the \emph{HTML preview} feature of the editor.
This feature was originally intended to simply display static HTML documents in
the editor, but soon started being used by extension developers to
implement complex interfaces, mainly due to flexibility and ease of use of HTML.
The HTML preview was not intended for these type of use cases, consequently this
approach had its downsides, the two most important ones being the fact that only
static contents could be displayed (requiring a full refresh for each
update) and that there was no way for the panel to interact with the rest of
the editor. When the first debugger was built, this was the only way of
extending Visual Studio Code's interface.

Around the time this project started, the \emph{Webview
API}\footnote{https://code.visualstudio.com/docs/extensions/webview} was being
developed by Microsoft as a successor for the HTML preview panel, with the aim
of providing a way for extension developers to extend the editor's interface,
similar to what they were doing with the HTML preview, but with an API designed
from the beginning to do so. The main feature of this new API is a way for the
panel to communicate back with extensions, making it possible to build
interactive interfaces. The Webview API was introduced in the April 2018 release
of VS Code, and we decided to use it for implementing the main panel of our
debugger.

Note that, whereas the original debugger used VS Code's native debugger API to
interface with the editor, we decided not to use it, as it would have provided
little benefits. The API was conceived to support more traditional debuggers,
with features that are not relevant for our project.

The debugger panel is a \emph{NodeJS}\footnote{\url{https://nodejs.org/}} module
separate from the rest of the extension. When the debugger extension needs to
create a Webview panel for the debugger, it points to the `debugger.html' file
(implementing the interface and logic of the panel) which is run as
a stand-alone process.

The debugger extension and the panel communicate by message-passing through the
\lstinline{Webview.postMessage} method (for messages from the debugger extension
to the panel) and the \lstinline{VSCode.postMessage} method (for messages from
the panel to the extension). These are simply wrappers around the
\lstinline{window.postMessage()} method used to enable cross-origin
communication between window objects in browsers.\footnotemark The debugger
extension sends messages to the panel to notify a focus change on the selected
verification state, to provide a new heap graph to be displayed, or to provide
various diagnostic messages. The panel sends messages to the debugger extension
only when it needs to notify that a new verification state has been selected.

\footnotetext{\url{https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage}}

