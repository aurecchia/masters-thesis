\chapter{Conclusion and Future Work}\label{sec:conclusion-and-future-work}

This project started out as a successor to the previous debugger project by
Kälin. The original goals were to expand on what had already been done by adding
support for quantified permissions, and then expand the editor with
a series of features for more easily debugging verification failures. After
a while it became obvious that we would have needed a different way of obtaining
instances of our model in order to maintain them manageable in size and to be
able to quickly add new facts in order to focus on more specific situations.
Thus, we began exploring the idea of turning the information we had about the
failing symbolic state into an Alloy model, in order to be able to generate
instances of our program. After that, the project turned into feasibility study
on whether counterexamples could be easily obtained via Alloy and the objective
of building an actual usable tool was put aside.

In this thesis, we explored an approach for the creation of an advanced
debugging tool for symbolic execution and implemented it in a proof of concept
integrated with the existing Viper IDE.

Though the current implementation is just a proof of concept and not all ideas
have been implemented (see \Cref{sec:limitations}), we think it provides
evidence that this approach is worth investigating further, to fix its
limitations.

The modeling approach we devised allows encoding the information from
a symbolic state in the symbolic execution trace into an Alloy model, which can
be then run to generate concrete instances of the program's state. This allows
the visualization of program examples, but most importantly, the visualization
of counterexamples, when the verification has failed.

This modeling technique has been implemented in a tool, integrated with the
Viper IDE, which allows inspecting the information available to Silicon during
the verification of a Viper program, and to automatically generate
a visualizations of the program's symbolic states, without leaving the editor.
The current implementation allows visualizing predicates and functions like the
previous debugger, but is additionally able to visualize objects defined via
quantified permissions.

In order to enable the implementation of the new tool and at the same time
maintain the system extensible, we designed and implemented a new architecture
which integrates the already-existing Viper extension with the new tool. In this
new architecture, the debugger and the main extension are completely separate.
The Viper IDE is unaware of the internals of the debugger, and can support other
extensions that interact with it at the same time.

Moreover, we updated the backend to provide the additional information that is
required for encoding all aspects of the programs symbolic state for the
generation of counterexamples. This information is made available along with
symbolic execution trace. The delivery mechanism of the trace itself has been
completely changed, so that now all the information is delivered via ViperServer
as a JSON object.


\section{Future Work}\label{sec:future-work}

In this section we briefly discuss some topics which may be interesting to
explore in future work on the debugger. These include both extensions to the
modeling technique and to the implementation of the tool.

\begin{description}
  \tightlist
  \item[Addressing current limitations] Of course, the most obvious and
    important topic for future work would be to address the limitations of both the
    modeling approach and of the current implementation discussed in
    \Cref{sec:limitations}.
  \item[Approximation] Both in \Cref{sec:approach} and in \Cref{sec:limitations}
    we discussed the need for approximations and consequently for oracle in the
    pipeline, that could distinguish spurious counterexample candidates from
    actual counterexamples. One clear topic for further work is therefore to add
    support for approximations both in the approach and in the actual pipeline
    of the system.
  \item[Support for user-provided constraints] As outlined in
    \Cref{sec:approach}, our technique conceptually allows for additional
    constraints to be added to the model. We think an interesting idea to
    explore would be that of enabling the user to add new constraints for the
    purpose of narrowing the counterexample search space to a more specific
    subset or to exclude situations that may not be of interest.
  \item[Modeling multiple states] This report describes an approach for
    translating a single verification state into an Alloy model. Viper allows
    using \viper{old} and labelled \viper{old} expressions to refer to the value
    of heap-dependent expressions at previous point in time. Silicon keeps track
    of the old heap and resolves labelled \viper{old} expressions to a symbolic
    value for us, but it's not clear how multiple states could be modeled
    together and at the same time kept consistent with each other. One problem
    is certainly that, in the symbolic execution trace, we have no distinction
    between a symbolic value from the current state and one that was
    ``retrieved'' from an old state.
    An idea to support this could be to exploit the \alloy{orderings} module
    provided by Alloy, which allows building dynamic models where multiple
    symbolic states are represented and related to each other. One important
    consideration to make is, given that our end-goal is that of building
    small \textbf{visual}, is how to represent the information from multiple
    verification states in an easy to understand and compact way.
  \item[Exploration of counterexample space] While the space explored by Alloy
    is bounded, there still might be many instances that satisfy the constraints
    of a model. This means that there might be different counterexamples to
    present to the user. The alloy API that we use in the ViperServer backend
    allows searching for the ``next'' instance, once a first one has been found.
    It would be interesting to investigate ways to integrate this mechanism into
    the debugger. One important challenge to solve, when researching this
    feature, is that of devising a way to prevent Alloy from generating multiple
    instances which only differ slightly one from the other.
\end{description}

