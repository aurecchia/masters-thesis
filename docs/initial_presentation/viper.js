/*
Language: TypeScript
Author: Panu Horsmalahti <panu.horsmalahti@iki.fi>
Contributors: Ike Ku <dempfi@yahoo.com>
Description: TypeScript is a strict superset of JavaScript
Category: scripting
*/

var viper = function(hljs) {
  var KEYWORDS = {
    keyword:
      ['function axiom import predicate method domain',
       'assume inhale',
       'var returns field',
       'define',
       'if else elseif while statelabel label goto',
       'acc forperm forall exists folding unforlding packaging applying',
       'old perm result let',
       'assert inhale exhale assume fold unfold package apply wand fresh constrainig',
       'requires ensures invariant',
       'new',
       'union in intersection setminus subset'
      ].join(" "),
    literal:
      'true false null write none wildcard'
    // built_in:
    //   'eval isFinite isNaN parseFloat parseInt decodeURI decodeURIComponent ' +
    //   'encodeURI encodeURIComponent escape unescape Object Function Boolean Error ' +
    //   'EvalError InternalError RangeError ReferenceError StopIteration SyntaxError ' +
    //   'TypeError URIError Number Math Date String RegExp Array Float32Array ' +
    //   'Float64Array Int16Array Int32Array Int8Array Uint16Array Uint32Array ' +
    //   'Uint8Array Uint8ClampedArray ArrayBuffer DataView JSON Intl arguments require ' +
    //   'module console window document any number boolean string void Promise'
  };

  return {
    aliases: ['vpr'],
    keywords: KEYWORDS,
    contains: [
      {
        className: 'meta',
        begin: /^\s*['"]use strict['"]/
      },
      hljs.APOS_STRING_MODE,
      hljs.QUOTE_STRING_MODE,
      { // template string
        className: 'string',
        begin: '`', end: '`',
        contains: [
          hljs.BACKSLASH_ESCAPE,
          {
            className: 'subst',
            begin: '\\$\\{', end: '\\}'
          }
        ]
      },
      hljs.C_LINE_COMMENT_MODE,
      hljs.C_BLOCK_COMMENT_MODE,
      hljs.COMMENT(/\.\.\./, '$'),
      {
        className: 'number',
        variants: [
          { begin: '\\b(0[bB][01]+)' },
          { begin: '\\b(0[oO][0-7]+)' },
          { begin: hljs.C_NUMBER_RE }
        ],
        relevance: 0
      },
      {
        className: 'function',
        beginKeywords: 'function',
        end: /[:\n;]/,
        excludeEnd: true,
        contains: [
          hljs.inherit(hljs.TITLE_MODE, {begin: /[A-Za-z$_][0-9A-Za-z$_]*/}),
          {
            className: 'params',
            begin: /\(/, end: /\)/,
            excludeBegin: true,
            excludeEnd: true,
            keywords: KEYWORDS,
            contains: [
              hljs.C_LINE_COMMENT_MODE,
              hljs.C_BLOCK_COMMENT_MODE
            ],
            illegal: /["'\(]/
          }
        ]
      },
      {
        className: 'method',
        beginKeywords: 'method',
        end: /[:\n;]/,
        excludeEnd: true,
        contains: [
          hljs.inherit(hljs.TITLE_MODE, {begin: /[A-Za-z$_][0-9A-Za-z$_]*/}),
          {
            className: 'params',
            begin: /\(/, end: /\)/,
            excludeBegin: true,
            excludeEnd: true,
            keywords: KEYWORDS,
            contains: [
              hljs.C_LINE_COMMENT_MODE,
              hljs.C_BLOCK_COMMENT_MODE
            ],
            illegal: /["'\(]/
          }
        ]
      },
      {
        begin: '\\.' + hljs.IDENT_RE, relevance: 0 // hack: prevents detection of keywords after dots
      },
      {
        className: 'meta', begin: '@[A-Za-z]+'
      }
    ]
  };
}
